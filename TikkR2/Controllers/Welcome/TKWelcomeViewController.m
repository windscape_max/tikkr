//
//  TKWelcomeViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKWelcomeViewController.h"
#import "WTUnderlinedLabel.h"
#import "TKAppDelegate.h"

#import "TKStorageService.h"
#import "TKServiceManager.h"
#import "TKNetworkService.h"

@interface TKWelcomeViewController ()

@property (weak, nonatomic) IBOutlet WTUnderlinedLabel *lblLogin;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@end

@implementation TKWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTap:)];
    [self.lblLogin addGestureRecognizer:self.tapGesture];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.lblLogin.drawLine = YES;
    self.lblLogin.userInteractionEnabled = YES;
}


#pragma mark -
#pragma mark - Actions

- (void)actionTap:(UITapGestureRecognizer*)recognizer {
    [self performSegueWithIdentifier:@"LoginSegue" sender:self];
}

- (IBAction)actionSignUpWithFacebook:(id)sender {
    [[[UIApplication sharedApplication] keyWindow] setRootViewController:self];
    __weak typeof(self) wself = self;
    [[[TKServiceManager sharedManager] networkService] authWithFBAndCompletion:^(BOOL success) {
        if (success) {
            [TKServiceManager sharedManager].storageService.registerViaService = @"GOOGLE";
            [wself performSegueWithIdentifier:@"PhoneNumberSegue" sender:wself];
        }
    }];
}

- (IBAction)actionSignUpWithGoogle:(id)sender {
    __weak typeof(self) wself = self;
    [[[TKServiceManager sharedManager] networkService] authWithGoogleAndCompletion:^(BOOL success) {
        if (success) {
            [TKServiceManager sharedManager].storageService.registerViaService = @"GOOGLE";
            [wself performSegueWithIdentifier:@"PhoneNumberSegue" sender:wself];
        }
    } inViewController:self];
}

- (IBAction)actionSignUpWithEmail:(id)sender {
    [TKServiceManager sharedManager].storageService.registerViaService = @"EMAIL";
    [self performSegueWithIdentifier:@"PhoneNumberSegue" sender:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




@end
