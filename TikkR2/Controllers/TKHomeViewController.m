//
//  TKHomeViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKHomeViewController.h"
#import "TKControllerDecorator.h"

@interface TKHomeViewController ()

@end

@implementation TKHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[TKControllerDecorator sharedDecorator] ApplyNavigationBarWithIconDesignWithNavigationController:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
