//
//  TKPasscodeViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKPasscodeViewController.h"
#import "WTUnderlinedTextField.h"
#import "TKServiceManager.h"
#import "TKUserModel.h"
#import "TKHelper.h"
#import "TKStorageService.h"
#import "TKTouchIDUtility.h"

@interface TKPasscodeViewController ()

@property (nonatomic, strong) TKTouchIDUtility *utility;
@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtPassCode;

@end

@implementation TKPasscodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.utility) {
        _utility = [TKTouchIDUtility new];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChangeOneCI:) name:UITextFieldTextDidChangeNotification object:self.txtPassCode];
    
    self.txtPassCode.hidden = NO;
    self.txtPassCode.disablePaste = YES;
    self.txtPassCode.keyboardAppearance = UIKeyboardAppearanceDark;
    self.txtPassCode.autocorrectionType = NO;
    self.txtPassCode.keyboardType = UIKeyboardTypeNumberPad;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showTouchID];
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.txtPassCode.text];
}

- (void)showTouchID {
    if ([TKServiceManager sharedManager].storageService.useTouchID) {
        [_utility EnableTouchIDWithCompletion:^(enum LAError err) {
            if ((int)err == -333) {
                [UIApplication sharedApplication].keyWindow.rootViewController = self.previousVC;
            }
        }];
    }
}

-(void)textFieldTextDidChangeOneCI:(NSNotification *)notification
{
    int limit = 4;
    if (self.txtPassCode.text.length == limit) {
        // check code correction
        [self.view endEditing:YES];
        
        NSString *pcode = [TKServiceManager sharedManager].currentUserModel.passCode;
        if ([pcode isEqualToString:self.txtPassCode.text]) {
            [UIApplication sharedApplication].keyWindow.rootViewController = self.previousVC;
        } else {
            self.txtPassCode.text = @"";
            [TKHelper ShowMessageWithTitle:@"Wrong passcode!"];
        }
    }
    
    if ([self.txtPassCode.text length] > limit) {
        [self.txtPassCode setText:[self.txtPassCode.text substringToIndex:limit]];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
