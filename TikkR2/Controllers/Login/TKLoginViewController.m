//
//  TKLoginViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKLoginViewController.h"
#import "WTUnderlinedLabel.h"
#import "WTUnderlinedTextField.h"

#import "TKServiceManager.h"
#import "TKNetworkService.h"
#import "TKHelper.h"
#import "TKStorageService.h"

#import "TKUserModel.h"
#import "TKUserLocalModel.h"
#import "UIButton+Selectable.h"

#define kANIM_TIME 1.4
#define kIMG_COEFF 40
#define K_INDICATOR_SIZE 1.8

@interface TKLoginViewController () <UITextFieldDelegate>
{
    BOOL keyboardIsShown;
    BOOL animAppled;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtEmail;
@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet WTUnderlinedLabel *lblSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithGoogle;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithFacebook;

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewBottomConstraint;

@end

@implementation TKLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTap:)];
    [self.lblSignUp addGestureRecognizer:self.tapGesture];
    
    [self.txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    self.txtEmail.keyboardAppearance = UIKeyboardAppearanceDark;
    self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.txtEmail reloadInputViews];
    
    self.btnLogin.layer.cornerRadius = 5;
    self.btnLoginWithFacebook.layer.masksToBounds = YES;
    self.btnLoginWithGoogle.layer.cornerRadius = 5;
    self.btnLoginWithGoogle.layer.masksToBounds = YES;
    self.btnLoginWithFacebook.layer.cornerRadius = 5;
    
    self.txtPassword.keyboardAppearance = UIKeyboardAppearanceDark;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    
    animAppled = NO;
    
    //self.txtEmail.text = @"t@t.t";
   // self.txtPassword.text = @"qwertyuio";
    self.txtEmail.delegate = self;
    self.txtPassword.delegate = self;
    
    self.btnLogin.selectedButtonColor = [UIColor grayColor];
    self.btnLogin.defaultButtonColor = self.btnLogin.backgroundColor;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.lblSignUp.drawLine = YES;
    self.lblSignUp.userInteractionEnabled = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtEmail) {
        [self.txtPassword becomeFirstResponder];
    }
    if (textField == self.txtPassword) {
        [self actionLogin:self];
    }
    return YES;
}


#pragma mark -
#pragma mark - Notifications

- (void)onKeyboardShow:(NSNotification *)notification
{
    keyboardIsShown = YES;
    if (!animAppled) {
        self.logoImageTopConstraint.constant -= 24;
        self.titleLabelTopConstraint.constant -= 12;
        self.stackViewBottomConstraint.constant += 36;
        [UIView animateWithDuration:kANIM_TIME animations:^(void){
            [self.view layoutIfNeeded];
            animAppled = YES;
        }];
    }
}

- (void)onKeyboardHide:(NSNotification *)notification
{
    keyboardIsShown = NO;
    if (animAppled) {
        self.logoImageTopConstraint.constant += 24;
        self.titleLabelTopConstraint.constant += 12;
        self.stackViewBottomConstraint.constant -= 36;
        [UIView animateWithDuration:kANIM_TIME animations:^(void){
            [self.view layoutIfNeeded];
            animAppled = NO;
        }];
    }
}


#pragma mark -
#pragma mark - Actions

- (void)actionTap:(UITapGestureRecognizer*)recognizer {
    [self performSegueWithIdentifier:@"SignUpSegue" sender:self];
}

- (IBAction)actionLogin:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self.txtEmail.text isEqualToString:@""]) {
        [TKHelper ShowMessageWithTitle:@"Type email!"];
        return;
    }
    
    if (![TKHelper isValidEmailAddress:self.txtEmail.text]) {
        [TKHelper ShowMessageWithTitle:@"Type valid email!"];
        return;
    }
    
    if ([self.txtPassword.text isEqualToString:@""]) {
        [TKHelper ShowMessageWithTitle:@"Type password!"];
        return;
    }
    
    [self.btnLogin SelectButton]; // select button color
    
    __weak typeof(self) wself = self;
    TKNetworkService *networkService = [[TKServiceManager sharedManager] networkService];
    [networkService authWithEmail:self.txtEmail.text andPassword:self.txtPassword.text andCompletion:^(BOOL sucess, id response) {
        [wself.btnLogin SelectButton]; // deselect button color
        
        if (sucess && response) {
            TKUserModel *model = [TKUserModel new];
            NSDictionary *dict = [[response objectForKey:@"object"] objectForKey:@"userId"];
            NSString *token = [[response objectForKey:@"object"]  objectForKey:@"token"];
            model = [EKMapper fillObject:model fromExternalRepresentation:dict withMapping:[TKUserModel objectMapping]];
            model.token = token;
            
            [TKServiceManager sharedManager].currentUserModel = model.copy;
            [[TKServiceManager sharedManager].networkService setToken:token.copy];
            
            // save data to local storage
            TKStorageService *storage = [TKServiceManager sharedManager].storageService;
            [storage setToken:token];
            [storage setPasscode:model.passCode];
            [storage setUseTouchID:YES];
            [storage StoreData];
            
            // go go go
            [wself performSegueWithIdentifier:@"HomeSegue" sender:wself];
            
        } else if (!sucess) {
            NSError *err = (NSError*)response;
            [TKHelper ShowMessageWithTitle:err.localizedDescription];
        }
    }];
}

- (IBAction)actionLoginWithFacebook:(id)sender {
    
}

- (IBAction)actionLoginWithGoogle:(id)sender {
    [[[TKServiceManager sharedManager] networkService] authWithGoogleAndCompletion:^(BOOL sucess) {
        
    } inViewController:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
