//
//  TKSplashViewController.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKSplashViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)skipToLogin:(id)sender;

@end
