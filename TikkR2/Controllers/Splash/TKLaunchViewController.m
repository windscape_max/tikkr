//
//  TKLaunchViewController.m
//  TikkR2
//
//  Created by Yura on 12/15/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKLaunchViewController.h"
#import "FadeSegue.h"

#define kAnimSpeed 0.7f
#define kAnimDelay 0.3f

@interface TKLaunchViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerYConstraint;

@end

@implementation TKLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- ( void )viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    __weak typeof(self) wself = self;
    self.imgLogo.translatesAutoresizingMaskIntoConstraints = YES;
    [UIView animateWithDuration:kAnimSpeed delay:kAnimDelay options:0
                     animations:^
     {
         CGRect frame = wself.imgLogo.frame;
   
         frame.origin.x = wself.view.center.x - 80;
         frame.origin.y = 75; // 75
         frame.size.width = 160;
         frame.size.height = 174;
         
         wself.imgLogo.frame = frame;
         [wself.view layoutIfNeeded];
     }
     completion:^( BOOL completed )
     {
         [wself performSegueWithIdentifier:@"SplashSegue" sender:wself];
     }];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //if ([segue.identifier isEqualToString:@"SplashSegue"]) {
        FadeSegue *segueEffect = (FadeSegue *)segue;
        segueEffect.animDuration = 0.4;
       // segueEffect.animateSegue = YES;
    //}
}


@end
