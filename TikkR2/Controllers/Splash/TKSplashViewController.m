//
//  TKSplashViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKSplashViewController.h"
#import "WTPageControl.h"

#define kNumberOfPages 4

@interface TKSplashViewController () <UIScrollViewDelegate>
{
    WTPageControl *pageControl;
}

@property (nonatomic, weak) IBOutlet UIView *pageView1;
@property (nonatomic, weak) IBOutlet UIView *pageView2;
@property (nonatomic, weak) IBOutlet UIView *pageView3;
@property (nonatomic, weak) IBOutlet UIView *pageView4;

@end

@implementation TKSplashViewController

@synthesize scrollView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSBundle mainBundle] loadNibNamed:@"PageView1" owner:self options:nil];
    [[NSBundle mainBundle] loadNibNamed:@"PageView2" owner:self options:nil];
    [[NSBundle mainBundle] loadNibNamed:@"PageView3" owner:self options:nil];
    [[NSBundle mainBundle] loadNibNamed:@"PageView4" owner:self options:nil];
    
    [self.pageView1 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self.pageView2 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self.pageView3 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    [self.pageView4 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    
    NSArray *pageViews = [[NSArray alloc] initWithObjects:self.pageView1, self.pageView2, self.pageView3, self.pageView4, nil];
    
    pageControl = [[WTPageControl alloc] init];
    [pageControl setBackgroundColor:[UIColor clearColor]];
    [pageControl setCenter:CGPointMake(self.view.center.x, self.view.bounds.size.height - 75.0f)];
    [pageControl setFrame:CGRectMake(pageControl.frame.origin.x, pageControl.frame.origin.y, 400.f, 250.f)];
    [pageControl setNumberOfPages:kNumberOfPages];
    [pageControl setCurrentPage:0];
    [pageControl setDefersCurrentPageDisplay:YES];
    [pageControl updateCurrentPageDisplay];
    
    [pageControl setImage:[UIImage imageNamed:@"active"] forPageIndex:0];
    [pageControl setImage:[UIImage imageNamed:@"inactive"] forPageIndex:1];
    
    [self.view addSubview:pageControl];
    
    [scrollView setFrame:CGRectMake(0.f, 0.f, self.view.frame.size.width, self.view.frame.size.height)];
    [scrollView setPagingEnabled:YES];
    [scrollView setBackgroundColor:[UIColor blackColor]];
    [scrollView setShowsHorizontalScrollIndicator:NO];
    scrollView.delegate = self;
    
    for (int i = 0; i < kNumberOfPages; i++)
    {
        // Determine the frame of the current page
        [[pageViews objectAtIndex:i] setFrame:CGRectMake(i * scrollView.frame.size.width, 0.0f, scrollView.frame.size.width, scrollView.frame.size.height)];
        [(UIView*)[pageViews objectAtIndex:i] setUserInteractionEnabled:YES];
        [scrollView addSubview:[pageViews objectAtIndex:i]];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height)];
}

#pragma mark -
#pragma mark UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGFloat pageWidth = scrollView.bounds.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger nearestNumber = lround(fractionalPage);
    
    if (pageControl.currentPage != nearestNumber)
    {
        pageControl.currentPage = nearestNumber;
        
        // If we are dragging, we want to update the page control directly during the drag
        if (scrollView.dragging) {
            [pageControl updateCurrentPageDisplay];
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
    // If we are animating (triggered by clicking on the page control), we update the page control
    [pageControl updateCurrentPageDisplay];
}

- (void)pageControlClicked:(id)sender {
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)skipToLogin:(id)sender {
    [self performSegueWithIdentifier:@"WelcomeSegue" sender:self];
}

@end

