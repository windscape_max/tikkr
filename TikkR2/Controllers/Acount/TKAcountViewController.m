//
//  TKAcountViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKAcountViewController.h"
#import "TKServiceManager.h"
#import "TKStorageService.h"
#import "TKWelcomeViewController.h"

@interface TKAcountViewController ()

@end

@implementation TKAcountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionLogout:(id)sender {
    [[[TKServiceManager sharedManager] storageService] ClearData];
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    TKWelcomeViewController *welcomeVC = [storyBoard instantiateViewControllerWithIdentifier:@"WelcomeVC"];
    window.rootViewController = welcomeVC;
    [window makeKeyAndVisible];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
