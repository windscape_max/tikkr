//
//  TKFinishMomentViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKFinishMomentViewController.h"
#import "TKServiceManager.h"
#import "TKUserModel.h"

@interface TKFinishMomentViewController ()
@property (weak, nonatomic) IBOutlet UIView *roundedView;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrowUp;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAge;
@property (weak, nonatomic) IBOutlet UILabel *lblUserGender;

@property (nonatomic, strong) UISwipeGestureRecognizer *swipeGesture;
@end

@implementation TKFinishMomentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.roundedView bringSubviewToFront:self.imgUserProfile];
    
    /*
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if ([[UserData sharedInstance] profileImage]) {
            self.imgUserProfile.image = [[UserData sharedInstance] profileImage];
        }
    });*/
    
    self.roundedView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    /*
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    
    NSDate *birthday = [formatter dateFromString:[[UserData sharedInstance] birthDay]];
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];*/
    NSInteger age = 37;
    
    self.lblUserAge.text = [NSString stringWithFormat:@"AGE: %li", (long)age];
    self.lblUserGender.text = [NSString stringWithFormat:@"GENDER: %@", @"Male"];
    self.lblUsername.text = [TKServiceManager sharedManager].currentUserModel.name;
    
    self.roundedView.userInteractionEnabled = YES;
    self.imgUserProfile.userInteractionEnabled = YES;
    self.imgArrowUp.userInteractionEnabled = YES;
    
    self.swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(actionSwipe:)];
    self.swipeGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.roundedView addGestureRecognizer:self.swipeGesture];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [NSTimer scheduledTimerWithTimeInterval:5
                                     target:self
                                   selector:@selector(animateView)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.imgUserProfile.layer.cornerRadius = self.imgUserProfile.frame.size.height / 2;
    self.imgUserProfile.layer.masksToBounds = YES;
    self.roundedView.layer.cornerRadius = 10;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)actionSwipe:(UISwipeGestureRecognizer*)recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionUp) {
        [self animateView];
    }
}

- (void)animateView {
    self.roundedView.translatesAutoresizingMaskIntoConstraints = YES;
    [UIView animateWithDuration:0.7
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
         CGRect rect = self.roundedView.frame;
         self.roundedView.frame = CGRectMake(rect.origin.x, (rect.size.height * 2) * -1,
                                             rect.size.width, rect.size.height);
     } completion:^(BOOL finished) {
         [self performSegueWithIdentifier:@"GoBackToMomentsVC" sender:self];
     }];
}

/*
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 if ([segue.identifier isEqualToString:@"GoToMomentsSegue"]) {
 MomentsTableViewController *vc = (MomentsTableViewController*)segue.destinationViewController;
 vc.refreshAvaluable = YES;
 }
 }*/

@end

