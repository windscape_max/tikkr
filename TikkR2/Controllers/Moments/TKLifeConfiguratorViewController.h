//
//  TKLifeConfiguratorViewController.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MomentData.h"
#import "AccidentData.h"

@interface TKLifeConfiguratorViewController : UIViewController

@property (nonatomic) BOOL editMode;
@property (nonatomic, strong) MomentData* selectedData;
@property (nonatomic, strong) AccidentData *mainData;
@property (nonatomic, strong) AccidentData *momentActiveData;

@end
