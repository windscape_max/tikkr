//
//  TKLifeConfiguratorViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKLifeConfiguratorViewController.h"

#import "LifeCoverConfiguratorView.h"
#import "WTTouchableScrollView.h"
#import "TKHelper.h"

#import "TKServiceManager.h"
#import "TKNetworkService.h"

@interface TKLifeConfiguratorViewController () <UIScrollViewDelegate, LiveCoverViewDelegate>
{
    UIColor* prevColor;
}

@property (weak, nonatomic) IBOutlet UIButton *btnFinish;
@property (weak, nonatomic) IBOutlet WTTouchableScrollView *scrollView;
@property (weak, nonatomic) LifeCoverConfiguratorView *contentView;

@end

#define K_CONST 400

@implementation TKLifeConfiguratorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    self.scrollView.userInteractionEnabled = YES;
    
    self.contentView = [[[NSBundle mainBundle] loadNibNamed:@"LifeCoverConfiguratorView" owner:self options:nil] objectAtIndex:0];
    //self.contentView.clipsToBounds = NO;
    self.contentView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.contentView.frame.size.height + K_CONST);
    self.contentView.backgroundColor = [UIColor clearColor];
    self.contentView.userInteractionEnabled = YES;
    [self.scrollView addSubview:self.contentView];
    [self.scrollView setContentSize:CGSizeMake(0, self.contentView.frame.size.height - K_CONST)];
    
    [self.contentView SetMomentData:self.selectedData];
    [self.contentView SetAccidentData:self.mainData];
    
    self.contentView.delegate = self;
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    
    prevColor = self.btnFinish.backgroundColor;
}

- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //self.scrollView.contentSize = CGSizeMake(0, 800);
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.scrollView.layer.cornerRadius = 10;
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionFinish:(id)sender {
    self.mainData = [self.contentView GetAccidentData];
    
    if (self.editMode) {
        [[[TKServiceManager sharedManager] networkService] updateUserMomentWithAccidentData:self.mainData andMoment:self.selectedData andCompletion:^(BOOL success, id response) {
            if (success && response) {
                [self performSegueWithIdentifier:@"FinishMomentsConfigSegue" sender:self];
            } else if (!success) {
                NSError *err = (NSError*)response;
                [TKHelper ShowMessageWithTitle:err.localizedDescription];
            }
        }];
        return;
    }
    
    // change moment data
    [[[TKServiceManager sharedManager] networkService] changeUserMomentWithMoment:self.mainData andCompletion:^(BOOL sucess, id response) {
        if (sucess && response) {
            [self performSegueWithIdentifier:@"FinishMomentsConfigSegue" sender:self];
        } else if (!sucess) {
            NSError *err = (NSError*)response;
            [TKHelper ShowMessageWithTitle:err.localizedDescription];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddPaymentSegue"]) {
        //AddPaymentMethods *destViewController = segue.destinationViewController;
        //destViewController.isMomentAdd = YES;
    }
}

- (void)didChangeSize:(CGSize)size {
    [self.scrollView setContentSize:CGSizeMake(0, self.contentView.frame.size.height - K_CONST)];
}

@end
