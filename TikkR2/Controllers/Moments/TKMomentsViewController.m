//
//  TKMomentsViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKMomentsViewController.h"
#import "TKAccidentConfiguratorViewController.h"
#import "TKMomentTableViewCell.h"
#import "TKSelMomentTableViewCell.h"

#import "TKMomentModel.h"
#import "TKActiveMomentModel.h"
#import "TKServiceManager.h"
#import "TKNetworkService.h"
#import "TKHelper.h"

#define kMomentCellIdentifier @"MomentTableViewCellIdentifier"
static NSString* kSelectedMomentCellIdentifier = @"SelMomentCellIdentifier";

@interface TKMomentsViewController () <UITableViewDelegate, UITableViewDataSource, TKSelMomentTableViewCellDelegate>
{
    NSInteger selectedIndex;
    BOOL editMode;
}

// general IB
@property (weak, nonatomic) IBOutlet UITableView *mainTable;

// table decoration views
@property (nonatomic, strong) UIView *headerUserMomentsView;
@property (nonatomic, strong) UIView *headerMomentsView;
@property (nonatomic, strong) UILabel *lblNotFoundMoments;
@property (nonatomic, strong) UIRefreshControl *tableRefresh;

// table data
@property (nonatomic, strong) NSMutableArray<TKMomentModel*> *moments;
@property (nonatomic, strong) NSMutableArray<TKActiveMomentModel*> *selectedMoments;

@end

@implementation TKMomentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // setup table view
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    self.mainTable.backgroundColor = [UIColor clearColor];
    //self.mainTable.frame = CGRectMake(0, 44, self.mainTable.frame.size.width, self.mainTable.frame.size.height);
    self.mainTable.userInteractionEnabled = YES;
    self.mainTable.multipleTouchEnabled = YES;
    
    [self.mainTable registerNib:[UINib nibWithNibName:@"TKMomentTableViewCell" bundle:nil] forCellReuseIdentifier:kMomentCellIdentifier];
    [self.mainTable registerNib:[UINib nibWithNibName:@"TKSelMomentTableViewCell" bundle:nil] forCellReuseIdentifier:kSelectedMomentCellIdentifier];
    
    // setup refresh control
    self.tableRefresh = [[UIRefreshControl alloc] init];
    [self.tableRefresh addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.mainTable addSubview:self.tableRefresh];
    
    // allocate memory for arrays
    self.selectedMoments = [NSMutableArray new];
    self.moments = [NSMutableArray new];
 
    //
    editMode = NO;
    
    [self refresh:self.tableRefresh];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}


#pragma mark -
#pragma mark - UITableView delegate & data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return (self.selectedMoments.count > 0) ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [tableView setContentInset:UIEdgeInsetsMake(12, 0, 30, 0)];
    if (self.selectedMoments.count > 0)
        return  (section == 0) ? self.selectedMoments.count : self.moments.count;
    else
        return self.moments.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0 && self.selectedMoments.count > 0)
        return 4;
    else
        return 32.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0 && self.selectedMoments.count > 0) {
       // TKSelectedMomentTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kSelectedMomentCellIdentifier];
       // return cell.frame.size.height;
        return 68.;
    } else {
        //TKMomentTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kMomentCellIdentifier];
       // return cell.frame.size.height;
        return 173.;
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    // create selected moments header view
    if (section == 0 && self.selectedMoments.count > 0) {
        if (!self.headerUserMomentsView) {
            self.headerUserMomentsView = [UIView new];
            self.headerUserMomentsView.backgroundColor = [UIColor clearColor];
        }
        return self.headerUserMomentsView;
    }
    
    // no data from request
    if (section == 0 && self.selectedMoments.count == 0 && self.moments.count == 0) {
        UIView *fakeHeaderView = [UIView new];
        fakeHeaderView.backgroundColor = [UIColor clearColor];
        return fakeHeaderView;
    }
    
    //
    if (section == 1 && self.moments.count == 0) {
        UIView *fakeHeaderView = [UIView new];
        fakeHeaderView.backgroundColor = [UIColor clearColor];
        [fakeHeaderView setFrame:CGRectMake(0, 0, self.mainTable.frame.size.width, [self tableView:self.mainTable heightForHeaderInSection:section])];
        
        if (!self.lblNotFoundMoments) {
            self.lblNotFoundMoments = [UILabel new];
            self.lblNotFoundMoments.text = @"Nothing to see here, all moments are set up!";
            UIFont *font = [UIFont fontWithName:@"Avenir" size: 15 ];
            self.lblNotFoundMoments.font = font;
            self.lblNotFoundMoments.textAlignment = NSTextAlignmentCenter;
            self.lblNotFoundMoments.textColor = [UIColor whiteColor];
            [self.view addSubview:self.lblNotFoundMoments];
        }
        
        // get table bottom
        TKSelMomentTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kSelectedMomentCellIdentifier];
        CGFloat topPos = (cell.frame.size.height * 3 ) + tableView.bounds.origin.y - 18;
        CGRect headerFrame = CGRectMake(0, ( tableView.frame.size.height - topPos) - [self tableView:tableView heightForHeaderInSection:section]
                                        , tableView.frame.size.width, 21);
        self.lblNotFoundMoments.frame = headerFrame;
        self.lblNotFoundMoments.hidden = NO;
        
        //[fakeHeaderView addSubview:lblCaption];
        fakeHeaderView.layer.borderColor = [UIColor clearColor].CGColor;
        fakeHeaderView.layer.borderWidth = 0;
        return fakeHeaderView;
    }
    
    self.lblNotFoundMoments.hidden = YES;
    
    UIImageView *titleView = [UIImageView new];
    titleView.backgroundColor = [UIColor clearColor];
    
    UILabel *lblCaption = [UILabel new];
    
    if (self.selectedMoments.count > 0)
        lblCaption.text = (section == 0) ? @"" : @"SET UP YOUR MOMENTS";
    else
        lblCaption.text = @"SET UP YOUR MOMENTS";
    
    if ([lblCaption.text isEqualToString:@""]) {
        [titleView setFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    } else {
        [titleView setFrame:CGRectMake(0, 0, tableView.frame.size.width, [self tableView:tableView heightForHeaderInSection:section])];
    }
    
    lblCaption.textColor = [UIColor whiteColor];
    UIFont *font = nil;
    if (self.selectedMoments.count > 0)
        font = [UIFont fontWithName:@"Avenir" size: (section == 1) ? 15 : 13 ];
    else
        font = [UIFont fontWithName:@"Avenir" size: 15 ];
    
    lblCaption.font = font;
    lblCaption.numberOfLines = 2;
    lblCaption.textAlignment = NSTextAlignmentCenter;
    
    if ([lblCaption.text isEqualToString:@"SET UP YOUR MOMENTS"]) {
        lblCaption.frame = CGRectMake(0, 0, titleView.frame.size.width, titleView.frame.size.height);
    } else {
        [lblCaption setFrame:titleView.bounds];
    }
    
    titleView.layer.borderColor = [UIColor clearColor].CGColor;
    titleView.layer.borderWidth = 0;
    
    [titleView addSubview:lblCaption];
    
    return titleView;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    /// SELECTED MOMENTS
    if (self.selectedMoments.count > 0 && indexPath.section == 0) {
        TKSelMomentTableViewCell *cell = (TKSelMomentTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"TKSelMomentTableViewCell" owner:self options:nil] objectAtIndex:0];
       // TKSelMomentTableViewCell *cell = (TKSelMomentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kSelectedMomentCellIdentifier];
        if (cell == nil) {
            cell = [[TKSelMomentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kSelectedMomentCellIdentifier];
        }
        
        TKActiveMomentModel *data = nil;
        NSInteger cellsCount = 0;
        
        if (self.selectedMoments.count > 0) {
            data = [self.selectedMoments objectAtIndex:indexPath.row];
            cellsCount = [self.selectedMoments count];
            cell.model = data;
            cell.model.momentId = data.momentId;
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        cell.activeFlag = [[self.selectedMoments objectAtIndex:indexPath.row] momentActive];
        cell.cellIndex = indexPath.row;
        cell.cellSection = indexPath.section;
        cell.canSwipe = YES;
        cell.contentView.backgroundColor = [UIColor clearColor];

        [cell UpdateUI];
        
        return cell;

    } else {  /// MOMENTS
        
        TKMomentTableViewCell *cell = (TKMomentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kMomentCellIdentifier];
        if (cell == nil) {
            cell = [[TKMomentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMomentCellIdentifier];
        }
        
        TKMomentModel *data = nil;
        NSInteger cellsCount = 0;
        
        if (self.moments.count > 0) {
            data = [self.moments objectAtIndex:indexPath.row];
            cellsCount = [self.moments count];
            cell.model = data;
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lineView.backgroundColor = (indexPath.row == cellsCount - 1) ? [UIColor clearColor] : [UIColor whiteColor];
        
        cell.canSwipe = NO;
        cell.backgroundColor = [UIColor clearColor];
        [cell updateUI];
        
        return cell;
    }

    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
    
    if (indexPath.section == 0 && self.selectedMoments.count > 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction* reconfigureButton = [UIAlertAction
                                            actionWithTitle:@"Reconfigure"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                editMode = YES;
                                                [self performSegueWithIdentifier:@"AccidentConfiguratorSegue" sender:self];
                                            }];
        
        UIAlertAction* removeButton = [UIAlertAction
                                       actionWithTitle:@"Remove"
                                       style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction * action) {
                                           
                                           NSString *momentId = [self.selectedMoments objectAtIndex:indexPath.row]._id;
                                           [[[TKServiceManager sharedManager] networkService] removeActiveMomentWithMomentId:momentId andCompletion:^(BOOL success, id response) {
                                               if (success && response) {
                                                   [self refresh:self.tableRefresh];
                                               } else if (!success){
                                                   NSError *err = (NSError*)response;
                                                   [TKHelper ShowMessageWithTitle:err.localizedDescription];
                                               }
                                           }];
                                           
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                       }];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        
        //Add your buttons to alert controller
        [alert addAction:reconfigureButton];
        [alert addAction:removeButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
         [self performSegueWithIdentifier:@"AccidentConfiguratorSegue" sender:self];
    }
}


#pragma mark -
#pragma mark - Actions

- (void)refresh:(UIRefreshControl*)control {
    
    [self.moments removeAllObjects];
    [self.selectedMoments removeAllObjects];
    
    __weak typeof(self) wself = self;
    [[[TKServiceManager sharedManager] networkService] getUserMomentsWithCompletion:^(BOOL sucess, id response) {
        
        if (sucess && response) {

            NSArray *arr = [response objectForKey:@"object"];
            for (NSDictionary *mod in arr) {
                TKMomentModel *model = [[TKMomentModel alloc] init];
                model = [EKMapper fillObject:model fromExternalRepresentation:mod withMapping:[TKMomentModel objectMapping]];
                [wself.moments addObject:model];
            }
            
            // get active moments
            [[[TKServiceManager sharedManager] networkService] getUserActiveMomentsWithCompletion:^(BOOL sucess, id response) {
                
                if (sucess && response) {
                    // map active moments
                    NSArray *arr = [response objectForKey:@"object"];
                    for (NSDictionary *mod in arr) {
                        TKActiveMomentModel *model = [[TKActiveMomentModel alloc] init];
                        model = [EKMapper fillObject:model fromExternalRepresentation:mod withMapping:[TKActiveMomentModel objectMapping]];
                        model.momentId = [(NSArray*)model.momentId objectAtIndex:0]; // life hack
                        /*
                        if ([model.momentId.name isEqualToString:@"Sports Moment"]) {
                            model.desc = @"Accident + Life + Sports Gear";
                        }
                        if ([model.momentId.name isEqualToString:@"Travel Moment"]) {
                            model.desc = @"Accident + Life + Luggage";
                        }
                        if ([model.momentId.name isEqualToString:@"Commute Moment"]) {
                            model.desc = @"Accident + Life + Personal Gear";
                        }*/
                        [wself.selectedMoments addObject:model];
                    }
                    
                } else if (!sucess) {
                    NSError *err = (NSError*)response;
                    [TKHelper ShowMessageWithTitle:err.localizedDescription];
                }
                
                [wself.tableRefresh endRefreshing]; // end refresh control
                [wself.mainTable reloadData];       // reload table when request complete
            }];
            
        } else if (!sucess) {
            NSError *err = (NSError*)response;
            [TKHelper ShowMessageWithTitle:err.localizedDescription];
        }
        
        [wself.tableRefresh endRefreshing]; // end refresh control
    }];
}



#pragma mark -
#pragma mark - Custom cell delegate

- (void)StatusChange:(TKSelMomentTableViewCell*)cell {
    
    [[[TKServiceManager sharedManager] networkService] setActiveMomentWithMomentId:cell.model._id andStatus:!cell.activeFlag ancCompletion:^(BOOL success, id response) {
        if (success && response) {
            [self refresh:self.tableRefresh];
        } else if (!success){
            NSError *err = (NSError*)response;
            [TKHelper ShowMessageWithTitle:err.localizedDescription];
        }
    }];
    
    /*
    [[self.selectedMoments objectAtIndex:cell.cellIndex] setActiveFlag:cell.activeFlag];
    if ([[UserData sharedInstance] registerViaFB]) {
        [[ServiceManager sharedManager] ChangeMomentFBStatusWithStatus:cell.activeFlag andMomentID:[[selectedMoments objectAtIndex:cell.cellIndex] momentID] andCompletion:^(IMFResponse* responce, NSError* error) {
        }];
    } else {
        [[ServiceManager sharedManager] ChangeMomentStatusWithStatus:cell.activeFlag andMomentID:[[selectedMoments objectAtIndex:cell.cellIndex] momentID] andCompletion:^(id response, NSError* error) {
        }];
    }
    
    [cell UpdateStatus];
    
    NSIndexPath *ip;
    for (int i = 0; i < selectedMoments.count; ++i) {
        ip = [NSIndexPath indexPathForRow:i inSection:0];
        SelectedMomentsTableViewCell* c = [self.tableView cellForRowAtIndexPath:ip];
        if (c != cell) {
            c.activeFlag = NO;
            [selectedMoments[i] setActiveFlag:NO];
            [c UpdateStatus];
            if ([[UserData sharedInstance] registerViaFB]) {
                [[ServiceManager sharedManager] ChangeMomentFBStatusWithStatus:NO andMomentID:[[selectedMoments objectAtIndex:i] momentID] andCompletion:^(IMFResponse* responce, NSError* error) {
                }];
            } else {
                [[ServiceManager sharedManager] ChangeMomentStatusWithStatus:NO andMomentID:[[selectedMoments objectAtIndex:i] momentID] andCompletion:^(id response, NSError* error) {
                }];
            }
        }
    }
    
    // resort array
    if (cell.activeFlag) {
        NSMutableArray* tempArray = [selectedMoments mutableCopy];
        [selectedMoments removeAllObjects];
        
        for (MomentData* moment in tempArray) {
            if (moment.activeFlag) {
                [selectedMoments addObject:moment];
            }
        }
        
        for (MomentData* moment in tempArray) {
            if (!moment.activeFlag) {
                [selectedMoments addObject:moment];
            }
        }
        
        NSMutableArray* ipArr = [NSMutableArray new];
        for (NSInteger i = 0; i <= cell.cellIndex; ++i) {
            NSIndexPath* ip = [NSIndexPath indexPathForRow:i inSection:0];
            [ipArr addObject:ip];
        }

        [self.tableView reloadRowsAtIndexPaths:[ipArr copy] withRowAnimation:UITableViewRowAnimationFade];
    }
*/
}

- (void)reloadActiveCellsWithoutAnimation {
    
    if (self.selectedMoments.count == 0) return;
    
    NSMutableArray* tempArray = [self.selectedMoments mutableCopy];
    [self.selectedMoments removeAllObjects];
    
    for (TKActiveMomentModel* moment in tempArray) {
        if (moment.momentActive) {
            [self.selectedMoments addObject:moment];
        }
    }
    
    for (TKActiveMomentModel* moment in tempArray) {
        if (!moment.momentActive) {
            [self.selectedMoments addObject:moment];
        }
    }
    
    [self.mainTable reloadData];
}


#pragma mark -
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AccidentConfiguratorSegue"]) {
        TKAccidentConfiguratorViewController *destViewController = segue.destinationViewController;
        if (editMode) {
            MomentData *data = [MomentData new];
            data.data = [AccidentData new];
            TKActiveMomentModel *model = [self.selectedMoments objectAtIndex:selectedIndex];

            data.name = model.momentId.name;
            data.insuranceFrom = (int)model.momentId.accident.from;
            data.insuranceTo = (int)model.momentId.accident.to;
            data.lifeInsuranceFrom = (int)model.momentId.insurance.from;
            data.lifeInsuranceTo = (int)model.momentId.insurance.to;
            data.payFrom = model.momentId.payInsurance.from;
            data.payTo = model.momentId.payInsurance.to;
            data.momentID = model._id;
            data.data.momentID = model._id;
            data.data._momentID = model._id;
            data.data.accidentPay = model.momentId.payAccident.from;
            data.data.lifePay = model.momentId.payInsurance.from;
            data.data.accidentInsurance = (int)model.momentId.payAccident.from;
            data.data.lifeInsurance = (int)model.momentId.payInsurance.from;
            data.data.momentActive = NO;
            data.price = model.accidentPay;
            data.data.accidentPay = model.accidentPay;
            data.data.accidentInsurance = (int)model.accidentInsurance;
            data.data.lifePay = model.lifePay;
            data.data.lifeInsurance = (int)model.lifeInsurance;
            data.data.disabilityByAccident = model.disabilityByAccident;
            data.data.hospitalization = model.hospitalization;
            data.data.deathByAccident = model.deathByAccident;
            data.data.smoke = model.smoke;
            data.data.disabilityAbsolute = model.disabilityAbsolute;
            data.data.totalDisability = model.totalDisability;
            data.data.funeralBenefit = model.funeralBenefit;
            data.data.momentActive = model.momentActive;
            
            destViewController.selectedMoment = data;
            destViewController.editMode = YES;
        } else {
            MomentData *data = [MomentData new];
            TKMomentModel *model = [self.moments objectAtIndex:selectedIndex];
            data.name = model.name;
            data.insuranceFrom = (int)model.accident.from;
            data.insuranceTo = (int)model.accident.to;
            data.lifeInsuranceFrom = (int)model.insurance.from;
            data.lifeInsuranceTo = (int)model.insurance.to;
            data.payFrom = model.payInsurance.from;
            data.payTo = model.payInsurance.to;
            data.momentID = model._id;
            data.data.momentID = model._id;
            data.data._momentID = model._id;
            data.data.accidentPay = model.payAccident.from;
            data.data.lifePay = model.payInsurance.from;
            data.data.accidentInsurance = (int)model.payAccident.from;
            data.data.lifeInsurance = (int)model.payInsurance.from;
            data.data.momentActive = NO;
            destViewController.selectedMoment = data;
            destViewController.editMode = NO;
        }
    }
}




@end
