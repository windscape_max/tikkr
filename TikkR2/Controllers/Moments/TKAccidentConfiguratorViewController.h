//
//  TKAccidentConfiguratorViewController.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKActiveMomentModel.h"
#import "TKMomentModel.h"
#import "MomentData.h"
#import "AccidentData.h"

@interface TKAccidentConfiguratorViewController : UIViewController
@property (nonatomic, strong) MomentData *selectedMoment;
@property (nonatomic, strong) AccidentData *mainData;
@property (nonatomic, strong) AccidentData *momentActiveData;
@property (nonatomic, assign) BOOL editMode;
@end
