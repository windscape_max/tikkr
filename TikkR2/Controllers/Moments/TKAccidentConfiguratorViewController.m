//
//  TKAccidentConfiguratorViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKAccidentConfiguratorViewController.h"
#import "TermsAndConditionsView.h"
#import "TKStorageService.h"
#import "TKServiceManager.h"
#import "TKNetworkService.h"
#import "TKUserModel.h"
#import "TKMomentRange.h"
#import "TKLifeConfiguratorViewController.h"

#import <KLCPopup.h>

@interface TKAccidentConfiguratorViewController ()
{
    float payConstant;
    NSUInteger index;
    NSInteger currentEditingValue;
    MomentData *newCalculationData;
}

@property (weak, nonatomic) IBOutlet UIView *roundedView;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAge;
@property (weak, nonatomic) IBOutlet UILabel *lblUserGender;
@property (weak, nonatomic) IBOutlet UIView *payView;
@property (weak, nonatomic) IBOutlet UILabel *lblPaySign;
@property (weak, nonatomic) IBOutlet UILabel *lblPayValue;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPrice;

@property (weak, nonatomic) IBOutlet UISlider *priceSlider;
@property (weak, nonatomic) IBOutlet UILabel *lblMinPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxPrice;
@property (weak, nonatomic) IBOutlet UIImageView *imgInsuranceInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgDeathByAccidentInfo;

@property (weak, nonatomic) IBOutlet UISwitch *switchTotalAndPermanent;
@property (weak, nonatomic) IBOutlet UISwitch *switchDailyAllowance;
@property (weak, nonatomic) IBOutlet UISwitch *switchDeathByAccident;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalAndPermanent;
@property (weak, nonatomic) IBOutlet UILabel *lblDailyAllowance;
@property (weak, nonatomic) IBOutlet UILabel *lblDeathByAccident;

@property (nonatomic, strong) UITapGestureRecognizer* tapForGreenInfo;
@property (nonatomic, strong) UITapGestureRecognizer* tapForWhiteInfo;
@property (nonatomic, strong) UITapGestureRecognizer* tapHideView;

@property (nonatomic, strong) TermsAndConditionsView *infoAlert;

@property (nonatomic) BOOL insuranceInfoActive;
@property (nonatomic) BOOL deathByAccidentInfoActive;

@property (nonatomic, strong) NSMutableArray *sliderNumbers;
@property (nonatomic, strong) NSMutableArray *sliderPayNumber;

@end

@implementation TKAccidentConfiguratorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.roundedView.layer.cornerRadius = 10;
    self.roundedView.backgroundColor = [UIColor whiteColor];
    self.roundedView.tintColor = [UIColor clearColor];
    self.roundedView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    
    /*
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    
    NSDate *birthday = [TKServiceManager sharedManager].currentUserModel.birthday;
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];*/
    NSInteger age = 37;
    
    self.lblUserAge.text = [NSString stringWithFormat:@"AGE: %li", (long)age];
    self.lblUserGender.text = [NSString stringWithFormat:@"GENDER: %@", @"Male"];
    self.lblUserName.text = [[TKServiceManager sharedManager].currentUserModel.name uppercaseString];
    /*
     if ([[UserData sharedInstance] profileImage]) {
     self.imgUserProfile.image = [[UserData sharedInstance] profileImage];
     }*/
    
    self.tapForGreenInfo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionShowInfo:)];
    [self.imgInsuranceInfo addGestureRecognizer:self.tapForGreenInfo];
    
    self.tapForWhiteInfo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionShowInfo:)];
    [self.imgDeathByAccidentInfo addGestureRecognizer:self.tapForWhiteInfo];
    
    //self.tapHideView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionHideView:)];
    
    self.imgInsuranceInfo.userInteractionEnabled = YES;
    self.imgDeathByAccidentInfo.userInteractionEnabled = YES;
    
    self.insuranceInfoActive = YES;
    self.deathByAccidentInfoActive = YES;
    
    self.infoAlert = [[[NSBundle mainBundle] loadNibNamed:@"TermsAndConditionsView" owner:self options:nil] objectAtIndex:0];
    
    // UPDATE DATA FROM MODEL
    self.sliderNumbers = [NSMutableArray new];
    self.sliderPayNumber = [NSMutableArray new];
    
    newCalculationData = [MomentData new];
    
    if (!self.editMode) {
        [self RecalculateArrays];
    }
    
    // As the slider moves it will continously call the -valueChanged:
    self.priceSlider.continuous = YES; // NO makes it call only once you let go
    self.priceSlider.value = 0;
    
    self.lblMaxPrice.text = [NSString stringWithFormat:@"€ %i", self.selectedMoment.insuranceTo];
    self.lblMinPrice.text = [NSString stringWithFormat:@"€ %i", self.selectedMoment.insuranceFrom];
    self.lblCurrentPrice.text = [NSString stringWithFormat:@"€ %i", self.selectedMoment.insuranceFrom];
    self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", self.selectedMoment.payFrom];
    
    self.mainData = [AccidentData new];
    
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
    [self.priceSlider addGestureRecognizer:gr];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.mainData.momentID = self.selectedMoment.momentID;
    if (self.editMode) {
        [self.switchDailyAllowance setOn:self.selectedMoment.data.hospitalization];
        [self.switchDeathByAccident setOn:self.selectedMoment.data.deathByAccident];
        [self.switchTotalAndPermanent setOn:self.selectedMoment.data.disabilityByAccident];
        
        // text hightlight
        if (self.switchTotalAndPermanent.on) {
            self.lblTotalAndPermanent.textColor = self.lblUserName.textColor;
        } else {
            self.lblTotalAndPermanent.textColor = [UIColor whiteColor];
        }
        if (self.switchDailyAllowance.on) {
            self.lblDailyAllowance.textColor = self.lblUserName.textColor;
        } else {
            self.lblDailyAllowance.textColor = [UIColor whiteColor];
        }
        if (self.switchDeathByAccident.on) {
            self.lblDeathByAccident.textColor = self.lblUserName.textColor;
            self.imgDeathByAccidentInfo.image = [UIImage imageNamed:@"info_details_green"];
        } else {
            self.lblDeathByAccident.textColor = [UIColor whiteColor];
            self.imgDeathByAccidentInfo.image = [UIImage imageNamed:@"info_details_white"];
        }
        
        [self RecalculateArrays];
        NSInteger i = 0;
        for (NSNumber* n in self.sliderNumbers) {
            if ([n intValue] == self.selectedMoment.data.accidentInsurance) {
                self.priceSlider.value = i;
                index = i;
            }
            i += 1;
        }
        
        self.lblCurrentPrice.text = [NSString stringWithFormat:@"€ %i", self.selectedMoment.data.accidentInsurance];
        self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", self.selectedMoment.data.accidentPay];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#define K_COEFF 25
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.infoAlert.frame = CGRectMake(K_COEFF, K_COEFF, self.view.bounds.size.width - K_COEFF * 2, self.view.bounds.size.height - K_COEFF * 2);
    self.infoAlert = (TermsAndConditionsView*)[self roundCornersOnView:(UIView*)self.infoAlert onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:10.0];
}

- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.imgUserProfile.layer.cornerRadius = self.imgUserProfile.frame.size.height / 2;
    self.imgUserProfile.layer.masksToBounds = YES;
    
    self.payView = [self roundCornersOnView:self.payView onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10.0];
}

#pragma mark -
#pragma mark - Calculations

- (void)RecalculateArrays {
    // remove objects
    [self.sliderPayNumber removeAllObjects];
    [self.sliderNumbers removeAllObjects];
    
    // get steps for slider
    for (int i = self.selectedMoment.insuranceFrom; i <= self.selectedMoment.insuranceTo; i += 500) {
        [self.sliderNumbers addObject:@(i)];
    }
    
    // check switch's
    if (self.switchDailyAllowance.on && self.switchTotalAndPermanent.on) {
        // pay constant calculations
        payConstant = self.selectedMoment.payTo - self.selectedMoment.payFrom;
        payConstant = payConstant / ([self.sliderNumbers count] - 1);
        
        // pay array calculations
        float f = 0;
        for (int i = 0; i < self.sliderNumbers.count; ++i) {
            [self.sliderPayNumber addObject:[NSNumber numberWithFloat:(self.selectedMoment.payFrom + f)]];
            f = f + payConstant;
        }
    }
    
    if (!self.switchDailyAllowance.on && !self.switchTotalAndPermanent.on) {
        // - 60%
        newCalculationData.payTo = self.selectedMoment.payTo * 0.4;
        newCalculationData.payFrom = self.selectedMoment.payFrom * 0.4;
        
        payConstant = newCalculationData.payTo - newCalculationData.payFrom;
        payConstant = payConstant / ([self.sliderNumbers count] - 1);
        
        // pay array calculations
        float f = 0;
        for (int i = 0; i < self.sliderNumbers.count; ++i) {
            [self.sliderPayNumber addObject:[NSNumber numberWithFloat:(newCalculationData.payFrom + f)]];
            f = f + payConstant;
        }
    }
    
    // %%%%%%%%%%%%%%%%%%%s
    if (self.switchTotalAndPermanent.on && !self.switchDailyAllowance.on) {
        // - 20 %
        newCalculationData.payTo = self.selectedMoment.payTo * 0.8;
        newCalculationData.payFrom = self.selectedMoment.payFrom * 0.8;
        
        payConstant = newCalculationData.payTo - newCalculationData.payFrom;
        payConstant = payConstant / ([self.sliderNumbers count] - 1);
        
        // pay array calculations
        float f = 0;
        for (int i = 0; i < self.sliderNumbers.count; ++i) {
            [self.sliderPayNumber addObject:[NSNumber numberWithFloat:(newCalculationData.payFrom + f)]];
            f = f + payConstant;
        }
    }
    
    if (!self.switchTotalAndPermanent.on && self.switchDailyAllowance.on) {
        // - 20 %
        newCalculationData.payTo = self.selectedMoment.payTo * 0.8;
        newCalculationData.payFrom = self.selectedMoment.payFrom * 0.8;
        
        payConstant = newCalculationData.payTo - newCalculationData.payFrom;
        payConstant = payConstant / ([self.sliderNumbers count] - 1);
        
        // pay array calculations
        float f = 0;
        for (int i = 0; i < self.sliderNumbers.count; ++i) {
            [self.sliderPayNumber addObject:[NSNumber numberWithFloat:(newCalculationData.payFrom + f)]];
            f = f + payConstant;
        }
    }
    
    
    
    // configurate slider
    NSInteger numberOfSteps = ((float)[self.sliderNumbers count] - 1);
    self.priceSlider.maximumValue = numberOfSteps;
    self.priceSlider.minimumValue = 0;
    
    // update current values in labels
    self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", [self.sliderPayNumber[index] floatValue]];
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionNext:(id)sender {
    // KEKEKEKEKE
    self.mainData.deathByAccident = self.switchDeathByAccident.on;
    self.mainData.hospitalization = self.switchDailyAllowance.on;
    self.mainData.disabilityByAccident = self.switchTotalAndPermanent.on;
    
    self.mainData.accidentPay = [self.lblPayValue.text floatValue];
    self.mainData.accidentInsurance = [self.sliderNumbers[index] intValue];
    
    [self performSegueWithIdentifier:@"LifeCoverConfiguratorSegue" sender:self];
}

- (IBAction)actionPriceChange:(id)sender {
    
    index = (NSUInteger)(self.priceSlider.value + 0.5);
    [self.priceSlider setValue:index animated:NO];
    NSNumber *number = self.sliderNumbers[index]; // <-- This numeric value you want
    float pay = [self.sliderPayNumber[index] floatValue];
    
    self.lblCurrentPrice.text = [NSString stringWithFormat:@"€ %i", [number intValue]];
    self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", pay];
}

- (IBAction)actionTotalAndPermanentSwitch:(id)sender {
    [self RecalculateArrays];
    if (self.switchTotalAndPermanent.on) {
        self.lblTotalAndPermanent.textColor = self.lblUserName.textColor;
    } else {
        self.lblTotalAndPermanent.textColor = [UIColor whiteColor];
    }
}

- (IBAction)actionDailyAllowanceSwitch:(id)sender {
    [self RecalculateArrays];
    if (self.switchDailyAllowance.on) {
        self.lblDailyAllowance.textColor = self.lblUserName.textColor;
    } else {
        self.lblDailyAllowance.textColor = [UIColor whiteColor];
    }
}

- (IBAction)actionDeathByAccidentSwitch:(id)sender {
    if (self.switchDeathByAccident.on) {
        self.lblDeathByAccident.textColor = self.lblUserName.textColor;
        self.imgDeathByAccidentInfo.image = [UIImage imageNamed:@"info_details_green"];
    } else {
        self.lblDeathByAccident.textColor = [UIColor whiteColor];
        self.imgDeathByAccidentInfo.image = [UIImage imageNamed:@"info_details_white"];
    }
}

- (void)actionShowInfo:(UITapGestureRecognizer*)recognizer {
    [[KLCPopup popupWithContentView:self.infoAlert showType:KLCPopupShowTypeFadeIn dismissType:KLCPopupDismissTypeFadeOut maskType:KLCPopupMaskTypeNone dismissOnBackgroundTouch:YES dismissOnContentTouch:NO] show];
}

- (void)sliderTapped:(UIGestureRecognizer *)g {
    UISlider* s = (UISlider*)g.view;
    if (s.highlighted)
        return; // tap on thumb, let slider deal with it
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    [s setValue:value animated:YES];
    
    index = (NSUInteger)(self.priceSlider.value + 0.5);
    [self.priceSlider setValue:index animated:NO];
    NSNumber *number = self.sliderNumbers[index]; // <-- This numeric value you want
    float pay = [self.sliderPayNumber[index] floatValue];
    
    self.lblCurrentPrice.text = [NSString stringWithFormat:@"€ %i", [number intValue]];
    self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", pay];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"LifeCoverConfiguratorSegue"]) {
        TKLifeConfiguratorViewController* vc = (TKLifeConfiguratorViewController*)segue.destinationViewController;
        vc.selectedData = self.selectedMoment;
        vc.mainData = self.mainData;
        vc.editMode = self.editMode;
    }
}


@end
