//
//  TKVerifyOfficialIDViewController.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKVerifyOfficialIDViewController : UIViewController

@property (nonatomic, strong) UIImage *scanIDImage;

@end
