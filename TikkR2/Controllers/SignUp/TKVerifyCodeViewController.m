//
//  TKVerifyCodeViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKVerifyCodeViewController.h"
#import "TKControllerDecorator.h"
#import "WTPasscodeView.h"
#import "TKHelper.h"

#import "TKServiceManager.h"
#import "TKNetworkService.h"
#import "TKStorageService.h"

#import "TKUserModel.h"

#import "UIButton+Selectable.h"

#define K_INDICATOR_SIZE 1.8

@interface TKVerifyCodeViewController () <WTPasscodeViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnResendCode;

@property (weak, nonatomic) IBOutlet UILabel *labelFirst;
@property (weak, nonatomic) IBOutlet UILabel *labelSecond;
@property (weak, nonatomic) IBOutlet WTPasscodeView *passCodeView;

@property (nonatomic) NSInteger charCount;
@property (nonatomic) NSInteger charIndexL;
@property (nonatomic) NSInteger charIndexR;
@property (nonatomic, strong) NSString* codeStr;

@property (nonatomic, strong) NSTimer *resendTimer;

@end

@implementation TKVerifyCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[TKControllerDecorator sharedDecorator] ApplyNavigationBarWithIconDesignWithNavigationController:self];
    
    self.charCount = 0;
    self.charIndexL = 0;
    self.charIndexR = 0;
    self.codeStr = @"";
    self.passCodeView.userInteractionEnabled = YES;
    self.labelFirst.userInteractionEnabled = YES;
    self.labelSecond.userInteractionEnabled = YES;

    self.btnResendCode.selectedButtonColor = [UIColor grayColor];
    self.btnResendCode.defaultButtonColor = self.btnResendCode.backgroundColor;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.passCodeView.passcodeDelegete = self;
    self.passCodeView.tintColor = [UIColor clearColor];
    [self.passCodeView addTarget:self action:@selector(editingChanged:) forControlEvents:UIControlEventEditingChanged];
    self.passCodeView.keyboardType = UIKeyboardTypeNumberPad;
    self.passCodeView.keyboardAppearance = UIKeyboardAppearanceDark;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void) reenableButton:(NSTimer*)timer {
    self.btnResendCode.enabled = YES;
    [self.btnResendCode SelectButton];
    [_resendTimer invalidate];
}


#pragma mark - 
#pragma mark - Actions

- (IBAction)actionResend:(id)sender {
    self.btnResendCode.enabled = NO;
    [TKHelper ShowMessageWithTitle:@"Re-send button will be enabled after 2 minutes"];
    [self.btnResendCode SelectButton];
    
    NSString *phone = [TKServiceManager sharedManager].currentUserModel.phone;
    [[[TKServiceManager sharedManager] networkService] regSendPhoneNumber:phone andCompletion:^(BOOL success, id response) {
        if (!success) {
            NSError *err = (NSError*)response;
            [TKHelper ShowMessageWithTitle:err.localizedDescription];
        }
    }];
    
    // resend code here
    _resendTimer = [NSTimer scheduledTimerWithTimeInterval:120.0
                                     target:self
                                   selector:@selector(reenableButton:)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void) editingChanged:(id) sender {
    ++self.charCount;
    
    if (self.charCount <= 3)
    {
        self.labelFirst.text = [self.labelFirst.text stringByReplacingCharactersInRange:NSMakeRange(self.charIndexL, 1) withString:((UITextField*)sender).text];
        //++self.charIndexL;
        self.charIndexL += 2;
    }
    else
    {
        self.labelSecond.text = [self.labelSecond.text stringByReplacingCharactersInRange:NSMakeRange(self.charIndexR, 1) withString:((UITextField*)sender).text];
        //++self.charIndexR;
        self.charIndexR += 2;
    }
    
    if (self.charCount > 5) [self checkCode:[NSString stringWithFormat:@"%@%s%@", self.labelFirst.text, " ", self.labelSecond.text]];
    ((UITextField*)sender).text = @"";
}

- (void)checkCode:(NSString *)code {
    self.charCount = 0;
    self.charIndexL = 0;
    self.charIndexR = 0;
    self.labelFirst.text = @"• • •";
    self.labelSecond.text = @"• • •";

    NSString *cStrg = [code stringByReplacingOccurrencesOfString:@" " withString:@""];

    // hide keyboard
    [self.view endEditing:YES];
    
    // request here
    __weak typeof(self) wself = self;
    NSString *phone = [[TKServiceManager sharedManager] currentUserModel].phone;
    [[[TKServiceManager sharedManager] networkService] regCheckCode:cStrg andPhone:phone andCompletion:^(BOOL success, id response) {
        if (success  && response) {
            [wself performSegueWithIdentifier:@"RegistrationSegue" sender:wself];
        } else if (!success) {
            NSError *err = (NSError*)response;
            [TKHelper ShowMessageWithTitle:err.localizedDescription];
        }
    }];
}


#pragma mark -
#pragma mark - WTPasscode delegate

- (void)textFieldDidDelete {
    self.charCount = self.charCount > 0 ? --self.charCount : 0;
    self.charIndexL = self.charIndexL >= 2 ? self.charIndexL : 2;
    
    if (self.charCount < 3)
    {
        self.charIndexL -= 2;
        NSString *resultString = [[NSString stringWithString:self.labelFirst.text] stringByReplacingCharactersInRange:NSMakeRange(self.charIndexL, 1) withString:@"•"];
        self.labelFirst.text = resultString;
    }
    else
    {
        self.charIndexR -= 2;
        NSString *resultString = [[NSString stringWithString:self.labelSecond.text] stringByReplacingCharactersInRange:NSMakeRange(self.charIndexR, 1) withString:@"•"];
        self.labelSecond.text = resultString;
    }
}

@end
