//
//  TKScanOfficialIDViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKScanOfficialIDViewController.h"
#import "TKControllerDecorator.h"
#import "TKVerifyOfficialIDViewController.h"

@interface TKScanOfficialIDViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIImage *scanedImage;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@property (weak, nonatomic) IBOutlet UILabel *lblScanOfficialID;
@property (weak, nonatomic) IBOutlet UILabel *lblScanOfficialIDDescr;

@end

@implementation TKScanOfficialIDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTap:)];
    self.lblScanOfficialID.userInteractionEnabled = YES;
    [self.lblScanOfficialID addGestureRecognizer:_tapGesture];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Verify Official ID";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationItem.title = @"Back";
}

#pragma mark -
#pragma mark - UIImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.scanedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self performSegueWithIdentifier:@"VerifyScanIDSegue" sender:self];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark - Actions

- (void)actionTap:(UITapGestureRecognizer*)recognizer {
    [self actionVerifyOfficialID:self];
}

- (IBAction)actionVerifyOfficialID:(id)sender {
    if (!_imagePicker) {
        self.imagePicker = [UIImagePickerController new];
        self.imagePicker.delegate = self;
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"VerifyScanIDSegue"]) {
        TKVerifyOfficialIDViewController *vc = (TKVerifyOfficialIDViewController*)[segue destinationViewController];
        vc.scanIDImage = self.scanedImage;
    }
}

@end
