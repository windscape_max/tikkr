//
//  TKRegistrationViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKRegistrationViewController.h"
#import "TKControllerDecorator.h"
#import "WTUnderlinedTextField.h"
#import "TKHelper.h"

#import "TKUserModel.h"
#import "TKUserLocalModel.h"
#import "TKServiceManager.h"

#import <MBFaker.h>

#define kANIM_TIME 0.4
#define kOFFSET_SIZE 30.f

@interface TKRegistrationViewController () <UITextFieldDelegate>

@property (nonatomic) BOOL animAppled;

@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtEmail;
@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtPassword;
@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtPasscode;
@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtConfirmPasscode;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topTitleConstraint;

@end

@implementation TKRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[TKControllerDecorator sharedDecorator] ApplyNavigationBarWithIconDesignWithNavigationController:self];
    
    self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtEmail.keyboardAppearance = UIKeyboardAppearanceDark;
    self.txtPassword.keyboardAppearance = UIKeyboardAppearanceDark;
    self.txtConfirmPassword.keyboardAppearance = UIKeyboardAppearanceDark;
    self.txtPasscode.keyboardAppearance = UIKeyboardAppearanceDark;
    self.txtConfirmPasscode.keyboardAppearance = UIKeyboardAppearanceDark;
    
    self.txtEmail.delegate = self;
    self.txtEmail.delegate = self;
    self.txtPassword.delegate = self;
    self.txtConfirmPassword.delegate = self;
    self.txtPasscode.delegate = self;
    self.txtConfirmPasscode.delegate = self;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fixTextLen:) name:@"UITextFieldTextDidChangeNotification" object:self.txtPasscode];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fixTextLen:) name:@"UITextFieldTextDidChangeNotification" object:self.txtConfirmPasscode];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UITextFieldTextDidChangeNotification" object:self.txtPasscode];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UITextFieldTextDidChangeNotification" object:self.txtConfirmPasscode];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtEmail) {
        [self.txtPassword becomeFirstResponder];
    }
    if (textField == self.txtPassword) {
        [self.txtConfirmPassword becomeFirstResponder];
    }
    if (textField == self.txtConfirmPassword) {
        [self.txtPasscode becomeFirstResponder];
    }
    if (textField == self.txtPasscode) {
        [self.txtConfirmPasscode becomeFirstResponder];
    }
    if (textField == self.txtConfirmPasscode) {
        [self actionScanOfficialID:self];
    }
    return YES;
}


#pragma mark -
#pragma mark - Notifications

- (void)onKeyboardShow:(NSNotification *)notification
{
    if (_animAppled) return;
    
    self.topTitleConstraint.constant -= kOFFSET_SIZE;
    [UIView animateWithDuration:kANIM_TIME animations:^(void){
        [self.view layoutSubviews];
        _animAppled = YES;
    }];
}

- (void)onKeyboardHide:(NSNotification *)notification
{
    if (!_animAppled) return;
    
    self.topTitleConstraint.constant += kOFFSET_SIZE;
    [UIView animateWithDuration:kANIM_TIME animations:^(void){
        [self.view layoutSubviews];
        _animAppled = NO;
    }];
}

- (void)fixTextLen:(NSNotification *)note {
    int limit = 4;
    if ([self.txtPasscode.text length] > limit) {
        [self.txtPasscode setText:[self.txtPasscode.text substringToIndex:limit]];
    }
    if ([self.txtConfirmPasscode.text length] > limit) {
        [self.txtConfirmPasscode setText:[self.txtConfirmPasscode.text substringToIndex:limit]];
    }
}


#pragma mark -
#pragma mark - Actions

- (IBAction)actionScanOfficialID:(id)sender {
    [self.view endEditing:YES];
    
    // check fields
    if ([_txtEmail.text isEqualToString:@""]) {
        [TKHelper ShowMessageWithTitle:@"Type email!"];
        return;
    }
    
    if (![TKHelper isValidEmailAddress:_txtEmail.text]) {
        [TKHelper ShowMessageWithTitle:@"Type valid email!"];
        return;
    }
    
    if ([_txtPassword.text isEqualToString:@""]) {
        [TKHelper ShowMessageWithTitle:@"Type password!"];
        return;
    }
    
    if (![_txtConfirmPassword.text isEqualToString:_txtPassword.text]) {
        [TKHelper ShowMessageWithTitle:@"The passwords you have entered do not match!"];
        return;
    }
    
    if ([_txtPasscode.text isEqualToString:@""]) {
        [TKHelper ShowMessageWithTitle:@"Type passcode!"];
        return;
    }

    if (![_txtConfirmPasscode.text isEqualToString:_txtPasscode.text]) {
        [TKHelper ShowMessageWithTitle:@"The passcodes you have entered do not match!"];
        return;
    }
    
    [[TKServiceManager sharedManager] currentUserModel].local.email = _txtEmail.text;
    [[TKServiceManager sharedManager] currentUserModel].local.password = _txtPassword.text;
    [[TKServiceManager sharedManager] currentUserModel].passCode = _txtPasscode.text;
    [[TKServiceManager sharedManager] currentUserModel].name = [MBFakerName name];
    [[TKServiceManager sharedManager] currentUserModel].birthday = [NSDate dateWithTimeIntervalSince1970:444443];
    
    // perform segue
    [self performSegueWithIdentifier:@"ScanOfficialIDSegue" sender:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
