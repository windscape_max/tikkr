//
//  TKPhoneNumberViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKPhoneNumberViewController.h"
#import "WTUnderlinedTextField.h"

#import "TKControllerDecorator.h"
#import "TKStorageService.h"
#import "TKServiceManager.h"
#import "TKNetworkService.h"
#import "TKHelper.h"

#import "TKWelcomeViewController.h"
#import "TKUserModel.h"

#import "UIButton+Selectable.h"

#define kANIM_TIME 0.4
#define kIMG_COEFF 40

@interface TKPhoneNumberViewController () <UINavigationControllerDelegate, UITextFieldDelegate>
{
    BOOL animAppled;
    CGRect old_rect;
    CGRect anim_rect;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet WTUnderlinedTextField *txtPhoneNumber;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterNumberLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneNumberTextTopConstraint;

@end

@implementation TKPhoneNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.txtPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
    self.txtPhoneNumber.keyboardAppearance = UIKeyboardAppearanceDark;
    [self.txtPhoneNumber reloadInputViews];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];

    [[TKControllerDecorator sharedDecorator] ApplyNavigationBarDesignWithNavigationController:self];
    
    _txtPhoneNumber.text = @"+";
    _txtPhoneNumber.delegate = self;
    
    self.btnNext.selectedButtonColor = [UIColor grayColor];
    self.btnNext.defaultButtonColor = self.btnNext.backgroundColor;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    old_rect = self.imgLogo.frame;
    anim_rect = CGRectMake(self.imgLogo.frame.origin.x + kIMG_COEFF / 2,
                           self.imgLogo.frame.origin.y - kIMG_COEFF / 2,
                           self.imgLogo.frame.size.width - kIMG_COEFF,
                           self.imgLogo.frame.size.height - kIMG_COEFF);
    self.navigationController.delegate = self;
    self.navigationController.navigationBarHidden = NO;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([viewController isKindOfClass:[TKWelcomeViewController class]]) { }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self actionNext:self];
    return YES;
}


#pragma mark -
#pragma mark - Notifications

- (void)onKeyboardShow:(NSNotification *)notification
{
    self.imgLogo.translatesAutoresizingMaskIntoConstraints = YES;
    [UIView animateWithDuration:kANIM_TIME animations:^(void){
        self.imgLogo.frame = anim_rect;
        animAppled = YES;
    }];
}

- (void)onKeyboardHide:(NSNotification *)notification
{
    [UIView animateWithDuration:kANIM_TIME animations:^(void){
        self.imgLogo.frame = old_rect;
        animAppled = NO;
    }];
}


#pragma mark -
#pragma mark - Actions

- (IBAction)actionNext:(id)sender {
    [_txtPhoneNumber resignFirstResponder];
    
    if (![TKHelper isValidPhoneNumber:_txtPhoneNumber.text]) {
        [TKHelper ShowMessageWithTitle:@"Phone number is not valid"];
        return;
    }
    
    [self.btnNext SelectButton];
    __weak typeof(self) wself = self;
    [[[TKServiceManager sharedManager] networkService] regSendPhoneNumber:_txtPhoneNumber.text andCompletion:^(BOOL success, id response) {
        [wself.btnNext SelectButton];
        if (success && response) {
            
            // save phone number for future
            [[TKServiceManager sharedManager] currentUserModel].phone = wself.txtPhoneNumber.text;
            
            // go go go
            [wself performSegueWithIdentifier:@"VerifyCodeSegue" sender:wself];
 
        } else if (!success) {
            NSError *err = (NSError*)response;
            //[TKHelper DismissHUD];
            [TKHelper ShowMessageWithTitle:err.localizedDescription];
        }
    }];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
