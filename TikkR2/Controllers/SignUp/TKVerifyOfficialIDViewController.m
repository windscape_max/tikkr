//
//  TKVerifyOfficialIDViewController.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKVerifyOfficialIDViewController.h"

#import "TKUserLocalModel.h"
#import "TKServiceManager.h"
#import "TKNetworkService.h"
#import "TKHelper.h"
#import "TKUserModel.h"
#import "TKStorageService.h"
#import "UIButton+Selectable.h"

#define K_INDICATOR_SIZE 1.8
#define K_ANIM_SPEED 0.43

@interface TKVerifyOfficialIDViewController ()

@property (nonatomic) BOOL acceptedId;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *scanImage;
@property (weak, nonatomic) IBOutlet UILabel *lblNationalCard;
@property (weak, nonatomic) IBOutlet UILabel *lblCardDescr;
@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadAndVerify;

@end

@implementation TKVerifyOfficialIDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.acceptedId = NO;
    self.mainView.layer.cornerRadius = 10;
    self.imgStatus.hidden = YES;
    self.imgStatus.translatesAutoresizingMaskIntoConstraints = YES;
    self.imgStatus.frame = CGRectMake(self.imgStatus.frame.origin.x, self.imgStatus.frame.origin.y, self.imgStatus.frame.size.width, 1);
   
    self.btnUploadAndVerify.selectedButtonColor = [UIColor grayColor];
    self.btnUploadAndVerify.defaultButtonColor = self.btnUploadAndVerify.backgroundColor;
    
    self.scanImage.image = _scanIDImage;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Verify Official ID";
}


#pragma mark -
#pragma mark - Actions

- (IBAction)actionUploadAndVerify:(id)sender {
    // put request here
    self.acceptedId = YES;
    
    if ([self.btnUploadAndVerify.titleLabel.text isEqualToString:@"Done"]) {
        
        self.acceptedId = NO;
        
        [self.btnUploadAndVerify SelectButton];
        __weak typeof(self) wself = self;
        TKUserModel *model = [[TKServiceManager sharedManager] currentUserModel];
        model.name = @"User Name";
        [[[TKServiceManager sharedManager] networkService] regUserRegisterWithUserModel:model andCompletion:^(BOOL success, id response) {
    
            if (success  && response) {
                [[[TKServiceManager sharedManager] networkService] authWithEmail:model.local.email andPassword:model.local.password andCompletion:^(BOOL sucess, id response) {
                    if (sucess && response) {
                        [wself.btnUploadAndVerify SelectButton];
                        
                        TKUserModel *_model = [TKUserModel new];
                        NSDictionary *dict = [[response objectForKey:@"object"] objectForKey:@"userId"];
                        NSString *token = [[response objectForKey:@"object"]  objectForKey:@"token"];
                        _model = [EKMapper fillObject:_model fromExternalRepresentation:dict withMapping:[TKUserModel objectMapping]];
                        _model.token = token;
                        
                        [TKServiceManager sharedManager].currentUserModel = _model.copy;
                        [[TKServiceManager sharedManager].networkService setToken:token.copy];
                        
                        // save data to local storage
                        TKStorageService *storage = [TKServiceManager sharedManager].storageService;
                        [storage setToken:token];
                        [storage setPasscode:_model.passCode];
                        [storage setUseTouchID:YES];
                        [storage StoreData];
                        
                        [wself performSegueWithIdentifier:@"HomeVCSegue" sender:wself];
                    } else if (!sucess) {
                        NSError *err = (NSError*)response;
                        [TKHelper ShowMessageWithTitle:err.localizedDescription];
                    }
                }];
                
            } else if (!success) {
                NSError *err = (NSError*)response;
                [TKHelper ShowMessageWithTitle:err.localizedDescription];
            }
        }];

    } else {
        if (self.acceptedId) {
            self.imgStatus.alpha = 0.f;
            self.imgStatus.hidden = NO;
            [UIView animateWithDuration:K_ANIM_SPEED animations:^(void) {
                self.imgStatus.frame = CGRectMake(self.imgStatus.frame.origin.x, self.imgStatus.frame.origin.y, self.scanImage.bounds.size.width / 1.7, self.scanImage.bounds.size.height);
                self.imgStatus.alpha = 1.f;
                [self.mainView layoutIfNeeded];
            } completion:^(BOOL finished) {
                [self.btnUploadAndVerify setTitle:@"Done" forState:UIControlStateNormal];
            }];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
