//
//  MomentData.h
//  TikkR
//
//  Created by Yura on 10/28/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "AccidentData.h"

@interface MomentData : NSObject

@property (nonatomic, strong)   NSString* name;
@property (nonatomic, strong)   UIImage* image;
@property (nonatomic) float     price;
@property (nonatomic) int       insuranceFrom;
@property (nonatomic) int       insuranceTo;
@property (nonatomic) int       lifeInsuranceFrom;
@property (nonatomic) int       lifeInsuranceTo;
@property (nonatomic) float     payFrom;
@property (nonatomic) float     payTo;
@property (nonatomic, strong)   NSString* momentID;
@property (nonatomic, strong)   NSString* momentDescr;
@property (nonatomic, strong)   NSNumber* savedPriceValue;
@property (nonatomic)           BOOL activeFlag;
@property (nonatomic, strong)   AccidentData* data;

@end
