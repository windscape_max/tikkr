//
//  AccidentData.h
//  TikkR
//
//  Created by Yura on 10/29/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccidentData : NSObject
// == momentId
// == userId
// == accidentPay
// == accidentInsurance
// == lifePay
// == lifeInsurance

// == disabilityByAccident
// == hospitalization
// == deathByAccident
// == smoke
// == disabilityAbsolute
// == totalDisability
// == funeralBenefit

@property (nonatomic, strong) NSString* _momentID;
@property (nonatomic, strong) NSString* momentID;
@property (nonatomic) float accidentPay;
@property (nonatomic) int accidentInsurance;
@property (nonatomic) float lifePay;
@property (nonatomic) int lifeInsurance;

@property (nonatomic) BOOL disabilityByAccident;
@property (nonatomic) BOOL hospitalization;
@property (nonatomic) BOOL deathByAccident;
@property (nonatomic) BOOL smoke;
@property (nonatomic) BOOL disabilityAbsolute;
@property (nonatomic) BOOL totalDisability;
@property (nonatomic) BOOL funeralBenefit;
@property (nonatomic) BOOL momentActive;

@property (nonatomic, strong) NSArray *beneficiaryName;
@property (nonatomic, strong) NSArray *beneficiaryNationalID;

@end
