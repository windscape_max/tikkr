//
//  MomentData.m
//  TikkR
//
//  Created by Yura on 10/28/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "MomentData.h"

@implementation MomentData

@synthesize image;
@synthesize price;
@synthesize name;
@synthesize insuranceTo;
@synthesize insuranceFrom;
@synthesize payTo;
@synthesize payFrom;
@synthesize momentID;
@synthesize lifeInsuranceTo;
@synthesize lifeInsuranceFrom;
@synthesize momentDescr;
@synthesize savedPriceValue;
@synthesize data;

@end
