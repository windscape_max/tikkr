//
//  TKSelectedMomentTableViewCell.h
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>


#define CELL_ACTIVE_COLOR [[UIColor colorWithRed:112.0/255.0 green:178.0/255.0 blue:123.0/255.0 alpha:1.0] colorWithAlphaComponent:0.5]
//#define CELL_DEACTIVE_COLOR [[UIColor colorWithRed:133/255 green:221/255 blue:177/255 alpha:1] colorWithAlphaComponent:0.1]
#define CELL_DEACTIVE_COLOR [UIColor colorWithRed:149.0/255.0 green:165.0/255.0 blue:166.0/255.0 alpha:1.0]

@class TKSelMomentTableViewCell;
@protocol TKSelMomentTableViewCellDelegate <NSObject>

- (void)StatusChange:(TKSelMomentTableViewCell*)cell;

@end

@class TKActiveMomentModel;

@interface TKSelMomentTableViewCell : UITableViewCell

@property (nonatomic, strong) TKActiveMomentModel *model;
@property (nonatomic) BOOL canSwipe;

@property (nonatomic, weak) id<TKSelMomentTableViewCellDelegate> delegate;
@property (nonatomic, strong) UISwipeGestureRecognizer *recognizerSwipe1;
@property (nonatomic) NSInteger cellIndex;
@property (nonatomic) NSInteger cellSection;
@property (nonatomic) BOOL activeFlag;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblCovers;
@property (weak, nonatomic) IBOutlet UILabel *lblActivate;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIView *mainView;

- (void)SetPriceData:(NSNumber*)price;
- (void)UpdateStatus;
- (void)UpdateUI;


@end
