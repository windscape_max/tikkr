//
//  TKRootWireframe.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKRootWireframe.h"
#import "TKPasscodeViewController.h"
#import "TKSplashViewController.h"
#import "TKServiceManager.h"
#import "TKStorageService.h"
#import "TKPasscodeViewController.h"

#import "TKNetworkConstants.h"
#import "TKNetworkService.h"
#import "TKActiveMomentModel.h"
#import "TKHelper.h"
#import "TKUserModel.h"
#import "TKUserLocalModel.h"

#import <IMFCore/IMFCore.h>
#import <IMFFacebookAuthentication/IMFFacebookAuthenticationHandler.h>
#import <FacebookSDK/FacebookSDK.h>

@interface TKRootWireframe ()

@property (nonatomic, weak) TKStorageService *storage;
@property (nonatomic, weak) UIStoryboard *storyBoard;

@end

@interface TKRootWireframe ()

@property (nonatomic, strong) UINavigationController *rootNavigationController;

@end

@implementation TKRootWireframe

- (void)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions andWindow:(UIWindow*)window {
    
    // activate bluemix sdk
    [[IMFClient sharedInstance] initializeWithBackendRoute:API_APP_ROUTE backendGUID:API_APP_GUID];
    [[IMFAuthorizationManager sharedInstance] initializeWithTenantId:API_APP_TENAT_ID];
    
    [FBAppEvents activateApp];
    [[IMFFacebookAuthenticationHandler sharedInstance] registerWithDefaultDelegate];
    
    // load UI
    self.storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];

    // startup conditions
    self.storage = [TKServiceManager sharedManager].storageService;
    [self.storage ReadData];
    
    // check if user already auth
    if (![self.storage IsDataValid]) {
        
        NSLog(@"show splash");
        
        // not valid data, maybe first app start
        // splash screen
        TKSplashViewController *splashVC = [self.storyBoard instantiateViewControllerWithIdentifier:@"LaunchVC"];
        window.rootViewController = splashVC;
        [window makeKeyAndVisible];
        
    } else {
        
        // splash screen
        TKSplashViewController *splashVC = [self.storyBoard instantiateViewControllerWithIdentifier:@"SplashVC"];
        window.rootViewController = splashVC;
        [window makeKeyAndVisible];
        
        // login here
        NSLog(@"login in app");
        
        // request
        __weak typeof(self) wself = self;
        NSString *token = [[TKServiceManager sharedManager] storageService].token;
        [[TKServiceManager sharedManager].networkService setToken:token.copy];
        
        NSLog(@"TOKEN :: %@", token);
        [[[TKServiceManager sharedManager] networkService] getUserInfoWithCompletion:^(BOOL sucess, id response) {
            if (sucess && response) {
                
                // map user
                TKUserModel *model = [TKUserModel new];
                NSDictionary *dict = [response objectForKey:@"object"];
                model = [EKMapper fillObject:model fromExternalRepresentation:dict withMapping:[TKUserModel objectMapping]];
                
                [TKServiceManager sharedManager].currentUserModel = model.copy; // save user data
                
                // show pass code vc
                if (wself.storage.passcode && ![wself.storage.passcode isEqualToString:@""] ) {
                    TKPasscodeViewController *passcodeVC = [wself.storyBoard instantiateViewControllerWithIdentifier:@"PasscodeVC"];
                    UINavigationController *homeVC = [wself.storyBoard instantiateViewControllerWithIdentifier:@"HomeNavigationVC"];
                    passcodeVC.previousVC = homeVC;
                    window.rootViewController = passcodeVC;
                    [window makeKeyAndVisible];
                }
                
            } else if (!sucess) {
                NSError *err = (NSError*)response;
                [TKHelper ShowMessageWithTitle:err.localizedDescription];
            }

        }];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application andWindow:(UIWindow*)window {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application andWindow:(UIWindow*)window {
    if (self.storage.passcode && self.storage.passcode.length > 0 && [TKServiceManager sharedManager].currentUserModel.name) {
        NSLog(@"show passcode");
        
        // passcode enabled, check it
        if (![window.rootViewController isKindOfClass:[TKPasscodeViewController class]]) {
            TKPasscodeViewController *passcodeVC = [self.storyBoard instantiateViewControllerWithIdentifier:@"PasscodeVC"];
            passcodeVC.previousVC = window.rootViewController;
            window.rootViewController = passcodeVC;
            [passcodeVC showTouchID];
        }
        
    } else {
        
        if (![TKServiceManager sharedManager].currentUserModel.token || [TKServiceManager sharedManager].currentUserModel.token.length == 0) return;
        
        // try authorize user
        __weak typeof(self) wself = self;
        
        [[[TKServiceManager sharedManager] networkService] getUserInfoWithCompletion:^(BOOL sucess, id response) {
            if (sucess && response) {
                
                // map user
                TKUserModel *model = [TKUserModel new];
                NSDictionary *dict = [[response objectForKey:@"object"] objectAtIndex:0];
                model = [EKMapper fillObject:model fromExternalRepresentation:dict withMapping:[TKUserModel objectMapping]];
                
                [TKServiceManager sharedManager].currentUserModel = model.copy; // save user data
                
                // show pass code vc
                if (wself.storage.passcode && ![wself.storage.passcode isEqualToString:@""] ) {
                    TKPasscodeViewController *passcodeVC = [wself.storyBoard instantiateViewControllerWithIdentifier:@"PasscodeVC"];
                    UINavigationController *homeVC = [wself.storyBoard instantiateViewControllerWithIdentifier:@"HomeNavigationVC"];
                    passcodeVC.previousVC = homeVC;
                    window.rootViewController = passcodeVC;
                }
                
            } else if (!sucess) {
                NSError *err = (NSError*)response;
                [TKHelper ShowMessageWithTitle:err.localizedDescription];
            }
            
        }];
        
    }
}

- (void)applicationWillTerminate:(UIApplication *)application andWindow:(UIWindow*)window {
    [self.storage StoreData];
}

@end
