//
//  TKControllerDecorator.h
//  TikkR2
//
//  Created by Yura on 11/22/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKControllerDecorator : NSObject

+ (id)sharedDecorator;

- (void)ApplyNavigationBarDesignWithNavigationController:(UIViewController*)controller;
- (void)ApplyNavigationBarWithIconDesignWithNavigationController:(UIViewController *)controller;
- (void)ApplyNavigationBarDesign3;

@end
