//
//  TKControllerDecorator.m
//  TikkR2
//
//  Created by Yura on 11/22/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKControllerDecorator.h"

@interface TKControllerDecorator ()

@property (nonatomic, strong) UIImage *titleViewImage;
@property (nonatomic, strong) UIColor *navigationBarColor;

@end

@implementation TKControllerDecorator

+ (id)sharedDecorator {
    static dispatch_once_t onceToken;
    static TKControllerDecorator *instance;
    dispatch_once(&onceToken, ^{
        if (!instance) {
            instance = [[self alloc] init];
            instance.titleViewImage = [UIImage imageNamed:@"header_logo"];
            instance.navigationBarColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
        }
    });
    return instance;
}


- (void)ApplyNavigationBarDesignWithNavigationController:(UIViewController *)controller {
    if (controller.navigationController.navigationBarHidden) {
        controller.navigationController.navigationBarHidden = NO;
    }
    controller.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    controller.navigationController.navigationBar.barTintColor = self.navigationBarColor;
    [controller.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)ApplyNavigationBarWithIconDesignWithNavigationController:(UIViewController *)controller {
    if (controller.navigationController.navigationBarHidden) {
        controller.navigationController.navigationBarHidden = NO;
    }
    controller.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    controller.navigationController.navigationBar.barTintColor = self.navigationBarColor;
    controller.navigationItem.titleView = [[UIImageView alloc] initWithImage:self.titleViewImage];
}

- (void)ApplyNavigationBarDesign3 {
    
}

@end
