//
//  TKTouchIDUtility.h
//  TikkR2
//
//  Created by Yura on 11/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LocalAuthentication/LAContext.h>
#import <LocalAuthentication/LAError.h>

@interface TKTouchIDUtility : NSObject

- (void)EnableTouchIDWithCompletion:(void(^)(enum LAError err))completion;

@end
