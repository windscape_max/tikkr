//
//  TKTouchIDUtility.m
//  TikkR2
//
//  Created by Yura on 11/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKTouchIDUtility.h"

@interface TKTouchIDUtility ()
@property (nonatomic, strong) LAContext *ctx;
@end

@implementation TKTouchIDUtility

- (void)EnableTouchIDWithCompletion:(void(^)(enum LAError err))completion {
    
    if (!self.ctx) {
        self.ctx = [[LAContext alloc] init];
    }
    
    NSError *error = nil;
    NSString *reason = [@"Unlock " stringByAppendingString:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];
    
    if ([self.ctx canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [self.ctx evaluatePolicy:LAPolicyDeviceOwnerAuthentication localizedReason:reason reply:^(BOOL success, NSError *error) {
            if (success) {
                // If an authentication was successful then define the app behavior below
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(-333);
                });
            } else {
                switch (error.code) {
                    case LAErrorAuthenticationFailed:
                        completion(LAErrorAuthenticationFailed);
                        break;
                    case LAErrorUserCancel:
                        completion(LAErrorUserCancel);
                        break;
                    case LAErrorUserFallback:
                        completion(LAErrorUserFallback);
                        completion(NO);
                        break;
                    case LAErrorSystemCancel:
                        completion(LAErrorSystemCancel);
                        break;
                    default:
                    {
                        completion(-300);
                    }
                }
            }
        }];
    } else {
        switch (error.code) {
            case LAErrorPasscodeNotSet:
                completion(LAErrorPasscodeNotSet);
                break;
            case LAErrorTouchIDNotAvailable:
                completion(LAErrorTouchIDNotAvailable);
                break;
            case LAErrorTouchIDNotEnrolled:
                completion(LAErrorTouchIDNotEnrolled);
                break;
            default:
            {
                completion(-300);
            }
        }
    }
}

@end
