//
//  FadeSegue.h
//  TikkR
//
//  Created by Ozzy on 10/13/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FadeSegue : UIStoryboardSegue

@property (nonatomic, assign) CGFloat animDuration;
@property (nonatomic, assign) BOOL    animateSegue;

@end
