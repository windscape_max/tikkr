//
//  FadeSegue.m
//  TikkR
//
//  Created by Ozzy on 10/13/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FadeSegue.h"

@implementation FadeSegue

- (void)perform
{
    CATransition *transition = [CATransition animation];
    transition.duration = (_animDuration == 0) ? 0.2f : _animDuration;
    transition.type = kCATransitionFade;
    
    [[self sourceViewController].view.window.layer addAnimation:transition forKey:kCATransitionFade];
    [[self sourceViewController] presentViewController:[self destinationViewController] animated:_animateSegue completion:NULL];
}

@end
