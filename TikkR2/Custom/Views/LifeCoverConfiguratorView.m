//
//  LifeCoverConfiguratorView.m
//  TikkR
//
//  Created by Yura on 11/1/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "LifeCoverConfiguratorView.h"
#import "TermsAndConditionsView.h"
#import "TKUserModel.h"
#import "TKServiceManager.h"
#import "TKStorageService.h"
#import "TKNetworkService.h"

//#import "UserData.h"

#import <KLCPopup.h>

#define K_COEFF 25

@interface LifeCoverConfiguratorView ()
{
    float payConstant;
    NSUInteger index;
    MomentData *newCalculationData;
    BOOL smoke;
    CGFloat topLevel;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAge;
@property (weak, nonatomic) IBOutlet UILabel *lblUserGender;
@property (weak, nonatomic) IBOutlet UILabel *lblPayValue;
@property (weak, nonatomic) IBOutlet UIImageView *imgInfoInsurance;
@property (weak, nonatomic) IBOutlet UISlider *sliderPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMinPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDoYouSmoke;
@property (weak, nonatomic) IBOutlet UIView *payView;

@property (nonatomic, strong) UITapGestureRecognizer* tapInfo1;
@property (nonatomic, strong) UITapGestureRecognizer* tapInfo2;
@property (nonatomic, strong) UITapGestureRecognizer* tapInfo3;
@property (nonatomic, strong) UITapGestureRecognizer* tapSmoke;

@property (nonatomic, strong) TermsAndConditionsView *infoAlert;

@property (nonatomic, strong) NSMutableArray *sliderNumbers;
@property (nonatomic, strong) NSMutableArray *sliderPayNumber;

@property (weak, nonatomic) IBOutlet UILabel *lblDeathAndDisability;

@property (weak, nonatomic) IBOutlet UIImageView *imgInfo2;
@property (weak, nonatomic) IBOutlet UIImageView *imgInfo3;

// bottom part
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalAndPermanent;
@property (weak, nonatomic) IBOutlet UILabel *lblFuneralBenefits;

@property (weak, nonatomic) IBOutlet UISwitch *switchFuneralBenefit;
@property (weak, nonatomic) IBOutlet UISwitch *switchTotalAndPermanent;
@property (weak, nonatomic) IBOutlet UISwitch *switchDeathAndDisability;

@property (weak, nonatomic) IBOutlet UIButton *btnAddBeneficiary;
@property (weak, nonatomic) IBOutlet UITextField *txtInsertName;
@property (weak, nonatomic) IBOutlet UITextField *txtNationalID;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewTopConstraint;

@end

@implementation LifeCoverConfiguratorView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    /*
     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
     [formatter setDateFormat:@"MM-dd-yyyy"];
     
     NSDate *birthday = [TKServiceManager sharedManager].currentUserModel.birthday;
     NSDate* now = [NSDate date];
     NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
     components:NSCalendarUnitYear
     fromDate:birthday
     toDate:now
     options:0];
     NSInteger age = [ageComponents year];*/
    NSInteger age = 37;
    
    self.lblUserAge.text = [NSString stringWithFormat:@"AGE: %li", (long)age];
    self.lblUserGender.text = [NSString stringWithFormat:@"GENDER: %@", @"Male"];
    self.lblUsername.text = [[TKServiceManager sharedManager].currentUserModel.name uppercaseString];
    /*
     if ([[UserData sharedInstance] profileImage]) {
     self.imgUserProfile.image = [[UserData sharedInstance] profileImage];
     }*/

    
    self.tapInfo1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionShowInfo:)];
    [self.imgInfoInsurance addGestureRecognizer:self.tapInfo1];
    self.imgInfoInsurance.userInteractionEnabled = YES;
    
    self.tapInfo2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionShowInfo:)];
    [self.imgInfo2 addGestureRecognizer:self.tapInfo2];
    self.imgInfo2.userInteractionEnabled = YES;
    
    self.tapInfo3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionShowInfo:)];
    [self.imgInfo3 addGestureRecognizer:self.tapInfo3];
    self.imgInfo3.userInteractionEnabled = YES;
    
    self.tapSmoke = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionSmoke:)];
    [self.lblDoYouSmoke addGestureRecognizer:self.tapSmoke];
    self.lblDoYouSmoke.userInteractionEnabled = YES;

    self.infoAlert = [[[NSBundle mainBundle] loadNibNamed:@"TermsAndConditionsView" owner:self options:nil] objectAtIndex:0];
    
    // UPDATE DATA FROM MODEL
    self.sliderNumbers = [NSMutableArray new];
    self.sliderPayNumber = [NSMutableArray new];
    
    newCalculationData = [MomentData new];
    
    self.switchFuneralBenefit.userInteractionEnabled = YES;
    
    smoke = YES;
    
    self.multipleTouchEnabled = YES;
    
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
    [self.sliderPrice addGestureRecognizer:gr];
    
    
}

- (void)SetMomentData:(MomentData*)moment {
    self.selectedMoment = moment;
    [self RecalculateArrays];
    
    // As the slider moves it will continously call the -valueChanged:
    self.sliderPrice.continuous = YES; // NO makes it call only once you let go
    self.sliderPrice.value = 0;
    
    self.lblMaxPrice.text = [NSString stringWithFormat:@"€ %i", self.selectedMoment.insuranceTo];
    self.lblMinPrice.text = [NSString stringWithFormat:@"€ %i", self.selectedMoment.insuranceFrom];
    self.lblCurrentPrice.text = [NSString stringWithFormat:@"€ %i", self.selectedMoment.insuranceFrom];
    self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", self.selectedMoment.payFrom];
    
    self.mainData = [AccidentData new];
    self.mainData.momentID = self.selectedMoment.momentID;
    
    self.infoAlert.frame = CGRectMake(K_COEFF, K_COEFF * 2, [UIScreen mainScreen].bounds.size.width - K_COEFF * 2, [UIScreen mainScreen].bounds.size.height - K_COEFF * 4);
    self.infoAlert = (TermsAndConditionsView*)[self roundCornersOnView:(UIView*)self.infoAlert onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:10.0];
    
    self.switchFuneralBenefit.userInteractionEnabled = YES;
    self.switchTotalAndPermanent.userInteractionEnabled = YES;
    self.switchDeathAndDisability.userInteractionEnabled = YES;
    self.userInteractionEnabled = YES;
    self.sliderPrice.userInteractionEnabled = YES;
    
    topLevel = self.txtNationalID.frame.origin.y + self.txtNationalID.frame.size.height + 14;
    
    if (self.selectedMoment.data) {
        // preload data
        smoke = self.selectedMoment.data.smoke;
        self.lblDoYouSmoke.text = (smoke) ? @"YES" : @"NO";
        
        [self.switchDeathAndDisability setOn:self.selectedMoment.data.disabilityAbsolute];
        [self.switchTotalAndPermanent setOn:self.selectedMoment.data.totalDisability];
        [self.switchFuneralBenefit setOn:self.selectedMoment.data.funeralBenefit];
        
        // text hightlight
        if (self.switchTotalAndPermanent.on) {
            self.lblTotalAndPermanent.textColor = self.lblUsername.textColor;
            self.imgInfo2.image = [UIImage imageNamed:@"info_details_green"];
        } else {
            self.lblTotalAndPermanent.textColor = [UIColor whiteColor];
            self.imgInfo2.image = [UIImage imageNamed:@"info_details_white"];
        }
        if (self.switchFuneralBenefit.on) {
            self.lblFuneralBenefits.textColor = self.lblUsername.textColor;
            self.imgInfo3.image = [UIImage imageNamed:@"info_details_green"];
        } else {
            self.lblFuneralBenefits.textColor = [UIColor whiteColor];
            self.imgInfo3.image = [UIImage imageNamed:@"info_details_white"];
        }
        if (self.switchDeathAndDisability.on) {
            self.lblDeathAndDisability.textColor = self.lblUsername.textColor;
        } else {
            self.lblDeathAndDisability.textColor = [UIColor whiteColor];
        }

        [self RecalculateArrays];
        NSInteger i = 0;
        for (NSNumber* n in self.sliderNumbers) {
            if ([n intValue] == self.selectedMoment.data.lifeInsurance) {
                self.sliderPrice.value = i;
                index = i;
            }
            i += 1;
        }
        
        self.lblCurrentPrice.text = [NSString stringWithFormat:@"€ %i", self.selectedMoment.data.lifeInsurance];
        self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", self.selectedMoment.data.lifePay];
    }
}

- (void)SetAccidentData:(AccidentData*)data {
    self.mainData = data;
}

- (AccidentData*)GetAccidentData {
    // FAKE DATA
    self.mainData.smoke = smoke;
    self.mainData.disabilityAbsolute = self.switchDeathAndDisability.on;
    self.mainData.totalDisability = self.switchTotalAndPermanent.on;
    self.mainData.funeralBenefit = self.switchFuneralBenefit.on;

    self.mainData.lifePay = [self.lblPayValue.text floatValue];
    self.mainData.lifeInsurance = [self.sliderNumbers[index] intValue];
    
    return self.mainData;
}

- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imgProfileImage.layer.cornerRadius = self.imgProfileImage.frame.size.height / 2;
    self.imgProfileImage.layer.masksToBounds = YES;
    
    self.payView = [self roundCornersOnView:self.payView onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10.0];
}

#pragma mark -
#pragma mark - Calculations

- (void)RecalculateArrays {
    // remove objects
    [self.sliderPayNumber removeAllObjects];
    [self.sliderNumbers removeAllObjects];
    
    // get steps for slider
    for (int i = self.selectedMoment.lifeInsuranceFrom; i <= self.selectedMoment.lifeInsuranceTo; i += 500) {
        [self.sliderNumbers addObject:@(i)];
    }
    
    if (self.switchDeathAndDisability.on) {
        // pay constant calculations
        payConstant = self.selectedMoment.payTo - self.selectedMoment.payFrom;
        payConstant = payConstant / ([self.sliderNumbers count] - 1);
        
        // pay array calculations
        float f = 0;
        for (int i = 0; i < self.sliderNumbers.count; ++i) {
            [self.sliderPayNumber addObject:[NSNumber numberWithFloat:(self.selectedMoment.payFrom + f)]];
            f = f + payConstant;
        }
    }
    
    if (!self.switchDeathAndDisability.on) {
        // - 40 %
        newCalculationData.payTo = self.selectedMoment.payTo * 0.6;
        newCalculationData.payFrom = self.selectedMoment.payFrom * 0.6;
        
        payConstant = newCalculationData.payTo - newCalculationData.payFrom;
        payConstant = payConstant / ([self.sliderNumbers count] - 1);
        
        // pay array calculations
        float f = 0;
        for (int i = 0; i < self.sliderNumbers.count; ++i) {
            [self.sliderPayNumber addObject:[NSNumber numberWithFloat:(newCalculationData.payFrom + f)]];
            f = f + payConstant;
        }
    }
    
    // configurate slider
    NSInteger numberOfSteps = ((float)[self.sliderNumbers count] - 1);
    self.sliderPrice.maximumValue = numberOfSteps;
    self.sliderPrice.minimumValue = 0;
    
    // update current values in labels
    if (index >= 0) {
       self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", [self.sliderPayNumber[index] floatValue]];
    }
}

- (void)actionShowInfo:(UITapGestureRecognizer*)recognizer {
    [[KLCPopup popupWithContentView:self.infoAlert showType:KLCPopupShowTypeFadeIn dismissType:KLCPopupDismissTypeFadeOut maskType:KLCPopupMaskTypeNone dismissOnBackgroundTouch:YES dismissOnContentTouch:NO] show];
}

- (IBAction)actionPriceChange:(id)sender {
    index = (NSUInteger)(self.sliderPrice.value + 0.5);
    [self.sliderPrice setValue:index animated:NO];
    NSNumber *number = self.sliderNumbers[index]; // <-- This numeric value you want
    float pay = [self.sliderPayNumber[index] floatValue];
    
    self.lblCurrentPrice.text = [NSString stringWithFormat:@"€ %i", [number intValue]];
    self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", pay];
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionTotalAndPermanentChange:(id)sender {
    if (self.switchTotalAndPermanent.on) {
        self.lblTotalAndPermanent.textColor = self.lblUsername.textColor;
        self.imgInfo2.image = [UIImage imageNamed:@"info_details_green"];
    } else {
        self.lblTotalAndPermanent.textColor = [UIColor whiteColor];
        self.imgInfo2.image = [UIImage imageNamed:@"info_details_white"];
    }
}

- (IBAction)actionTotalAndBenefitChange:(id)sender {
    if (self.switchFuneralBenefit.on) {
        self.lblFuneralBenefits.textColor = self.lblUsername.textColor;
        self.imgInfo3.image = [UIImage imageNamed:@"info_details_green"];
    } else {
        self.lblFuneralBenefits.textColor = [UIColor whiteColor];
        self.imgInfo3.image = [UIImage imageNamed:@"info_details_white"];
    }
}

- (IBAction)actionDeathAndDisabilityChange:(id)sender {
    [self RecalculateArrays];
    if (self.switchDeathAndDisability.on) {
        self.lblDeathAndDisability.textColor = self.lblUsername.textColor;
    } else {
        self.lblDeathAndDisability.textColor = [UIColor whiteColor];
    }
}

-(void)actionSmoke:(UITapGestureRecognizer*)recognizer {
    smoke = !smoke;
    if (smoke) {
        self.lblDoYouSmoke.text = @"YES";
    } else {
        self.lblDoYouSmoke.text = @"NO";
    }
}

- (IBAction)actionAddBeneficiary:(id)sender {
    static CGFloat localSize = 0;
    
    UITextField *txtInserName = [UITextField new];
    UITextField *txtNationalID = [UITextField new];
    
    txtInserName.placeholder = self.txtInsertName.placeholder;
    txtNationalID.placeholder = self.txtNationalID.placeholder;
    
    txtInserName.translatesAutoresizingMaskIntoConstraints = NO;
    txtInserName.frame = CGRectMake(self.txtNationalID.frame.origin.x,
                                    topLevel,
                                    self.txtNationalID.frame.size.width,
                                    self.txtNationalID.frame.size.height);
    
    // middle layer
    topLevel += txtInserName.frame.size.height + 8;
    
    txtNationalID.translatesAutoresizingMaskIntoConstraints = NO;
    txtNationalID.frame = CGRectMake(txtInserName.frame.origin.x,
                                    topLevel,
                                    txtInserName.frame.size.width,
                                    txtInserName.frame.size.height);
    NSLog(@"%f\n%f", txtInserName.frame.origin.x, txtInserName.frame.origin.y);
    // bottom layer
    topLevel += 14;
    localSize += (txtInserName.frame.size.height * 2 ) + 8 + 28;
    
    [self addSubview:txtInserName];
    [self addSubview:txtNationalID];
    
    self.bottomViewTopConstraint.constant += localSize;
    [self.delegate didChangeSize:self.frame.size];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)sliderTapped:(UIGestureRecognizer *)g {
    UISlider* s = (UISlider*)g.view;
    if (s.highlighted)
        return; // tap on thumb, let slider deal with it
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    [s setValue:value animated:YES];
    
    index = (NSUInteger)(self.sliderPrice.value + 0.5);
    [self.sliderPrice setValue:index animated:NO];
    NSNumber *number = self.sliderNumbers[index]; // <-- This numeric value you want
    float pay = [self.sliderPayNumber[index] floatValue];
    
    self.lblCurrentPrice.text = [NSString stringWithFormat:@"€ %i", [number intValue]];
    self.lblPayValue.text = [NSString stringWithFormat:@"%.2f", pay];
}

@end





