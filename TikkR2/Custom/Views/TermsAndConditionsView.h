//
//  TermsAndConditionsView.h
//  TikkR
//
//  Created by Yura on 10/30/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionsView : UIView
@property (weak, nonatomic) IBOutlet UITextView *txtMain;

@end
