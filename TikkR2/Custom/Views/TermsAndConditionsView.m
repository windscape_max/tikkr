//
//  TermsAndConditionsView.m
//  TikkR
//
//  Created by Yura on 10/30/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TermsAndConditionsView.h"

@implementation TermsAndConditionsView

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.txtMain setContentOffset:CGPointMake(0, 0) animated:NO];
    [self.txtMain layoutIfNeeded];
}

@end
