//
//  LifeCoverConfiguratorView.h
//  TikkR
//
//  Created by Yura on 11/1/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MomentData.h"
#import "AccidentData.h"

@protocol LiveCoverViewDelegate <NSObject>

- (void)didChangeSize:(CGSize)size;

@end

@interface LifeCoverConfiguratorView : UIView

@property (nonatomic, weak) id<LiveCoverViewDelegate> delegate;

@property (nonatomic, strong) MomentData* selectedMoment;
@property (nonatomic, strong) AccidentData* mainData;
@property (nonatomic, strong) AccidentData *momentActiveData;

- (void)SetMomentData:(MomentData*)moment;
- (void)SetAccidentData:(AccidentData*)data;
- (AccidentData*)GetAccidentData;


@end
