//
//  WTUnderlinedTextField.m
//
//  Created by Ozzy on 10/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "WTUnderlinedTextField.h"

@implementation WTUnderlinedTextField

- (void)layoutSubviews {
    [super layoutSubviews];
    [self drawRect:self.bounds];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5f;
    border.borderColor = [[UIColor alloc] initWithRed:241.f green:249.f blue:251.f alpha:1.f].CGColor;
    border.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, self.frame.size.height);
    border.borderWidth = borderWidth;
    [self.layer addSublayer:border];
    self.layer.masksToBounds = YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (self.disablePaste) {
        if (action == @selector(paste:))
            return NO;
    }
    return [super canPerformAction:action withSender:sender];
}

@end
