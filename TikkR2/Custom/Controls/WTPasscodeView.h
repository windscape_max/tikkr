//
//  WTPasscodeView.h
//  sdfsdf
//
//  Created by Ozzy on 10/14/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WTPasscodeViewDelegate <NSObject>
@optional - (void)textFieldDidDelete;

@end

@interface WTPasscodeView : UITextField<UIKeyInput>

@property (nonatomic, weak) id<WTPasscodeViewDelegate> passcodeDelegete;

@end
