//
//  UIButton+Selectable.m
//  TikkR2
//
//  Created by Yura on 12/9/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "UIButton+Selectable.h"
#import <objc/runtime.h>

@implementation UIButton (Selectable)

CATEGORY_PROPERTY_GET_SET(UIColor*, defaultButtonColor, setDefaultButtonColor:)
CATEGORY_PROPERTY_GET_SET(UIColor*, selectedButtonColor, setSelectedButtonColor:)

- (instancetype)init {
    self = [super init];
    if (self) {
        self.defaultButtonColor = self.backgroundColor;
    }
    return self;
}

- (void)SelectButton {
    self.isButtonSelected = !self.isButtonSelected;
    self.backgroundColor = (self.isButtonSelected) ? self.selectedButtonColor : self.defaultButtonColor;
}


- (BOOL)isButtonSelected {
    NSNumber *isBOOL = objc_getAssociatedObject(self, @selector(isButtonSelected));
    return isBOOL.boolValue;
}

- (void)setIsButtonSelected:(BOOL)isButtonSelected {
    NSNumber *isBOOL = [NSNumber numberWithBool:isButtonSelected];
    objc_setAssociatedObject(self, @selector(isButtonSelected), isBOOL, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
