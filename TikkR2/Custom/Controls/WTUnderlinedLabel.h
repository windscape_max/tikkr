//
//  WTUnderlinedLabel.h
//  TikkR
//
//  Created by Yura on 11/2/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTUnderlinedLabel : UILabel

@property (nonatomic) BOOL drawLine;

@end
