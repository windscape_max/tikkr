//
//  WTPageControl.h
//
//  Created by Ozzy on 10/18/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTPageControl : UIPageControl

@property (nonatomic, assign) NSInteger  numberOfPages;
@property (nonatomic, assign) NSInteger  currentPage;

@property (nonatomic, assign) BOOL       hidesForSinglePage;
@property (nonatomic, assign) BOOL       defersCurrentPageDisplay;

@property (nonatomic, retain) UIColor*   activeColor;
@property (nonatomic, retain) UIColor*   inactiveColor;

@property (nonatomic, assign) CGFloat    indicatorSize;
@property (nonatomic, assign) CGFloat    indicatorMargin;

- (id)initWithFrame:(CGRect)frame;

- (void)setImage:(UIImage *)image forPageIndex:(NSInteger)index;
- (UIImage *)imageForPageIndex:(NSInteger)pageIndex;

- (void)updateCurrentPageDisplay;
- (void)updateIndicators;

@end

// Support UIImage Category (you can move it in a separate file too)
@interface UIImage (UIImageTint)

- (UIImage *)imageTintedWithColor:(UIColor *)color;
- (UIImage *)imageTintedWithColor:(UIColor *)color fraction:(CGFloat)fraction;

@end
