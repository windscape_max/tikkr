//
//  WTPasscodeView.m
//  sdfsdf
//
//  Created by Ozzy on 10/14/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "WTPasscodeView.h"

@implementation WTPasscodeView

@synthesize passcodeDelegete;

- (void)deleteBackward {
    [super deleteBackward];
    if ([self.passcodeDelegete respondsToSelector:@selector(textFieldDidDelete)]) [self.passcodeDelegete textFieldDidDelete];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if (action == @selector(paste:)) {
        return NO;
    }
    return [super canPerformAction:action withSender:sender];
}

@end
