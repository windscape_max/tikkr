//
//  WTUnderlinedTextField.h
//
//  Created by Ozzy on 10/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTUnderlinedTextField : UITextField

@property (nonatomic) BOOL disablePaste;

@end
