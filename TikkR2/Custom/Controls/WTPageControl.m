//
//  WTPageControl.m
//
//  Created by Ozzy on 10/18/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "WTPageControl.h"

#define kDefaultIndicatorSize       6.0f
#define kDefaultIndicatorMargin     16.0f


@interface WTPageControl()
{
    NSMutableArray* imageArray;
}

@end

@implementation WTPageControl

@synthesize numberOfPages, currentPage, hidesForSinglePage, defersCurrentPageDisplay;
@synthesize activeColor, inactiveColor, indicatorSize, indicatorMargin;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupInitialProperties];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupInitialProperties];
}

- (void)setupInitialProperties {
    self.indicatorSize = kDefaultIndicatorSize;
    self.indicatorMargin = kDefaultIndicatorMargin;
    //self.activeColor = [UIColor colorWithWhite:1.0f alpha: 1.0f];
    //self.inactiveColor = [UIColor colorWithWhite:0.9f alpha: 0.5f];
}

- (void)setCurrentPage:(NSInteger)pageNumber {
    if (currentPage == pageNumber) return;
    currentPage = MIN(MAX(0, pageNumber), numberOfPages - 1);
    if (self.defersCurrentPageDisplay == NO) [self setNeedsDisplay];
}

- (void)setNumberOfPages:(NSInteger)newNumberOfPages {
    numberOfPages = newNumberOfPages;
    
    imageArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSInteger i = 0; i < numberOfPages; ++i) [imageArray addObject:[NSNull null]];
    
    currentPage = MIN(MAX(0, currentPage), numberOfPages - 1);
    self.bounds = self.bounds;
    [self setNeedsDisplay];
    
    [self setHidden:(newNumberOfPages < 2 && hidesForSinglePage)];
}

- (void)setHidesForSinglePage:(BOOL)hide {
    hidesForSinglePage = hide;
    [self setHidden:(self.numberOfPages < 2 && hidesForSinglePage)];
}

- (void)setDefersCurrentPageDisplay:(BOOL)defers {
    defersCurrentPageDisplay = defers;
}

- (void)setActiveColor:(UIColor *)color {
    activeColor = color;
    [self setNeedsDisplay];
}

- (void)setInactiveColor:(UIColor *)color {
    inactiveColor = color;
    [self setNeedsDisplay];
}

- (void)setIndicatorMargin:(CGFloat)margin {
    indicatorMargin = margin;
    self.bounds = self.bounds;
    [self setNeedsDisplay];
}

- (void)setFrame:(CGRect)frame {
    frame.size = [self sizeForNumberOfPages:numberOfPages];
    super.frame = frame;
}

- (void)setBounds:(CGRect)bounds {
    bounds.size = [self sizeForNumberOfPages:numberOfPages];
    super.bounds = bounds;
}

- (CGSize)sizeForNumberOfPages:(NSInteger)pageCount {
    return CGSizeMake(pageCount * self.indicatorSize + (pageCount - 1) * self.indicatorMargin + 44.0f, MAX(44.0f, self.indicatorSize + 4.0f));
}

- (void)updateCurrentPageDisplay {
    if (self.defersCurrentPageDisplay == NO) return;
    [self updateIndicators];
	[self setNeedsDisplay];
}

- (void)setImage:(UIImage *)image forPageIndex:(NSInteger)index {
    if (index >= self.numberOfPages) return;
    [imageArray replaceObjectAtIndex:index withObject:(image == nil ? [NSNull class] : image)];
    [self setNeedsDisplay];
}

- (UIImage *)imageForPageIndex:(NSInteger)index {
    if (index >= self.numberOfPages || [imageArray objectAtIndex:index] == [NSNull null]) return nil;
    return [imageArray objectAtIndex:index];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	// Get the touch location
	UITouch *touch = [touches anyObject];
	CGPoint touchLocation = [touch locationInView:self];
	
	// Check whether the touch is in the right or left hand-side of the control
    NSInteger newPage;
    
	if (touchLocation.x < (self.bounds.size.width / 2)) newPage = MAX(self.currentPage - 1, 0);
	else newPage = MIN(self.currentPage + 1, numberOfPages - 1);
    self.currentPage = newPage;
    
	[self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextSetAllowsAntialiasing(context, YES);

    CGFloat indicatorWidth = self.numberOfPages * self.indicatorSize + MAX(0, self.numberOfPages - 1) * self.indicatorMargin;
    CGPoint indicatorLocation = CGPointMake(CGRectGetMidX(self.bounds) - indicatorWidth / 2.0f, CGRectGetMidY(self.bounds) - self.indicatorSize / 2.0f);
    
    for (NSInteger i = 0; i < numberOfPages; ++i) {
        CGRect indicatorRect;
        if(i == self.currentPage) indicatorRect = CGRectMake(indicatorLocation.x - kDefaultIndicatorSize / 2, indicatorLocation.y, self.indicatorSize * 2, self.indicatorSize);
        else indicatorRect = CGRectMake(indicatorLocation.x, indicatorLocation.y, self.indicatorSize, self.indicatorSize);
        BOOL isSelectedIndicator = (i == self.currentPage);
        
        UIImage *indicatorImage = [[self imageForPageIndex:i] imageTintedWithColor:(isSelectedIndicator ? self.activeColor : self.inactiveColor)];
        if (indicatorImage == nil) [self drawDotIndicatorInContext:context withRect:indicatorRect isCurrentSelection:isSelectedIndicator]; // Use regular dot indicators
        else [indicatorImage drawInRect:CGRectInset(indicatorRect, -1.5f, -1.5f)]; // Use image defined indicators
        
        indicatorLocation.x += (self.indicatorSize + self.indicatorMargin);
    }
    CGContextRestoreGState(context);
}

- (void)drawDotIndicatorInContext:(CGContextRef)contextRef withRect:(CGRect)indicatorRect isCurrentSelection:(BOOL)isSelected {
    if (isSelected) {
            CGContextSetFillColorWithColor(contextRef, self.activeColor.CGColor);
            CGContextFillEllipseInRect(contextRef, CGRectInset(indicatorRect, -0.5f, -0.5f));
    } else {
        CGContextSetFillColorWithColor(contextRef, self.inactiveColor.CGColor);
        CGContextFillEllipseInRect(contextRef, indicatorRect);
    }
}

/* FIX ME: Should use already existing images in imageArray, instead of manually defined ones as it does now */
- (void)updateIndicators
{
    UIImage *active = [UIImage imageNamed:@"active"];
    UIImage *inactive = [UIImage imageNamed:@"inactive"];
    
    for (int i = 0; i < numberOfPages; ++i) [self setImage:(i == currentPage) ? active : inactive forPageIndex:(i)];
}

@end

@implementation UIImage (UIImageTint)

- (UIImage *)imageTintedWithColor:(UIColor *)color {
	return [self imageTintedWithColor:color fraction:0.0];
}

- (UIImage *)imageTintedWithColor:(UIColor *)color fraction:(CGFloat)fraction {
	if (color) {
		UIImage *image;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 40000
		if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0) {
			UIGraphicsBeginImageContextWithOptions([self size], NO, 0.0);
		}
#else
		if ([[[UIDevice currentDevice] systemVersion] floatValue] < 4.0) {
			UIGraphicsBeginImageContext([self size]);
		}
#endif
		CGRect rect = CGRectZero;
		rect.size = [self size];
		
		[color set];
		UIRectFill(rect);
        
		[self drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1.0];
		
		if (fraction > 0.0)
			[self drawInRect:rect blendMode:kCGBlendModeSourceAtop alpha:fraction];
		image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		return image;
	}
	return self;
}

@end
