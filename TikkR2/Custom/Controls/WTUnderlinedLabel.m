//
//  WTUnderlinedLabel.m
//  TikkR
//
//  Created by Yura on 11/2/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "WTUnderlinedLabel.h"

@implementation WTUnderlinedLabel

- (void)drawRect:(CGRect)rect {
    // Drawing code
    if (_drawLine)
    {
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSetRGBStrokeColor(ctx, 255, 255, 255, 1.0f); // RGBA
        CGContextSetLineWidth(ctx, 1.0f);
        
        CGContextMoveToPoint(ctx, 0, self.bounds.size.height - 1);
        CGContextAddLineToPoint(ctx, self.bounds.size.width, self.bounds.size.height - 1);
        
        CGContextStrokePath(ctx);
    }

    [super drawRect:rect];
}

@end
