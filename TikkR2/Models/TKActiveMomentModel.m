//
//  TKActiveMomentModel.m
//  TikkR2
//
//  Created by Yura on 11/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKActiveMomentModel.h"


@implementation TKActiveMomentModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.momentId = [TKMomentModel new];
        self.userId = [TKUserModel new];
        self.beneficiary = [TKMomentBeneficiary new];
    }
    return self;
}


+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"_id",
                                          @"funeralBenefit",
                                          @"totalDisability",
                                          @"disabilityAbsolute",
                                          @"smoke",
                                          @"deathByAccident",
                                          @"hospitalization",
                                          @"disabilityByAccident",
                                          @"lifeInsurance",
                                          @"lifePay",
                                          @"accidentInsurance",
                                          @"accidentPay",
                                          @"momentActive",
                                          @"desc"]];
        [mapping hasMany:[TKMomentModel class] forKeyPath:@"momentId"];
        [mapping hasOne:[TKUserModel class] forKeyPath:@"userId"];
        [mapping hasOne: [TKMomentBeneficiary class] forKeyPath:@"beneficiary"];
    }];
}

- (NSString*)description {
    NSMutableString *descr = [NSMutableString new];
    [descr appendFormat:@"\n_id %@\n", self._id];
    [descr appendFormat:@"funeralBenefit %d\n", self.funeralBenefit];
    [descr appendFormat:@"totalDisability %d\n", self.totalDisability];
    [descr appendFormat:@"disabilityAbsolute %d\n", self.disabilityAbsolute];
    [descr appendFormat:@"smoke %d\n", self.smoke];
    [descr appendFormat:@"deathByAccident %d\n", self.deathByAccident];
    [descr appendFormat:@"hospitalization %d\n", self.hospitalization];
    [descr appendFormat:@"disabilityByAccident %d\n", self.disabilityByAccident];
    [descr appendFormat:@"lifeInsurance %ld\n", self.lifeInsurance];
    [descr appendFormat:@"lifePay %f\n", self.lifePay];
    [descr appendFormat:@"accidentInsurance %ld\n", self.accidentInsurance];
    [descr appendFormat:@"accidentPay %f\n", self.accidentPay];
    [descr appendFormat:@"momentActive %d\n", self.momentActive];
    [descr appendFormat:@"momentId %@\n", self.momentId];
    [descr appendFormat:@"beneficiary %@\n", self.beneficiary];
    [descr appendFormat:@"userId %@\n", self.userId];
    [descr appendFormat:@"userId %@\n", self.desc];
    return descr;
}

@end
