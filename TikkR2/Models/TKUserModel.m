//
//  TKUserModel.m
//  TikkR2
//
//  Created by Yura on 11/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKUserModel.h"
#import "TKUserLocalModel.h"

@implementation TKUserModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.local = [[TKUserLocalModel alloc] init];
    }
    return self;
}

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"_id",
                                          @"birthday",
                                          @"name",
                                          @"passCode",
                                          @"phone",
                                          @"token"]];
        [mapping hasOne:[TKUserLocalModel class] forKeyPath:@"local"];
    }];
}

- (NSString*)description {
    NSMutableString *descr = [NSMutableString new];
    [descr appendFormat:@"\n_id %@\n", self._id];
    [descr appendFormat:@"birthday %@\n", self.birthday];
    [descr appendFormat:@"name %@\n", self.name];
    [descr appendFormat:@"passCode %@\n", self.passCode];
    [descr appendFormat:@"phone %@\n", self.phone];
    [descr appendFormat:@"local %@\n", self.local];
    [descr appendFormat:@"token %@\n", self.token];
    return descr;
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy set_id:[self._id copyWithZone:zone]];
        [copy setBirthday:[self.birthday copyWithZone:zone]];
        [copy setName:[self.name copyWithZone:zone]];
        [copy setPassCode:[self.passCode copyWithZone:zone]];
        [copy setPhone:[self.phone copyWithZone:zone]];
        [copy setToken:[self.token copyWithZone:zone]];
        [copy setLocal:[self.local copyWithZone:zone]];
    }
    
    return copy;
}

@end
