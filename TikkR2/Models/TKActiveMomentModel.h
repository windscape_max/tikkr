//
//  TKActiveMomentModel.h
//  TikkR2
//
//  Created by Yura on 11/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKMomentModel.h"
#import "TKMomentBeneficiary.h"
#import "TKUserModel.h"

@interface TKActiveMomentModel : NSObject<EKMappingProtocol>

@property (nonatomic, copy) NSString *_id;
@property (nonatomic) BOOL funeralBenefit;
@property (nonatomic) BOOL totalDisability;
@property (nonatomic) BOOL disabilityAbsolute;
@property (nonatomic) BOOL smoke;
@property (nonatomic) BOOL deathByAccident;
@property (nonatomic) BOOL hospitalization;
@property (nonatomic) BOOL disabilityByAccident;
@property (nonatomic) NSInteger lifeInsurance;
@property (nonatomic) float lifePay;
@property (nonatomic) NSInteger accidentInsurance;
@property (nonatomic) float accidentPay;
@property (nonatomic) BOOL momentActive;
@property (nonatomic, copy) NSString *desc;

@property (nonatomic, strong) TKMomentModel *momentId;
@property (nonatomic, strong) TKMomentBeneficiary *beneficiary;
@property (nonatomic, strong) TKUserModel *userId;


@end
