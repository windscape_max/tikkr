//
//  TKUserLocalModel.m
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKUserLocalModel.h"

@implementation TKUserLocalModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"email", @"password"]];
    }];
}

- (NSString*)description {
    NSMutableString *descr = [NSMutableString new];
    [descr appendFormat:@"\nemail %@\n", self.email];
    [descr appendFormat:@"password %@\n", self.password];
    return descr;
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setEmail:[self.email copyWithZone:zone]];
        [copy setPassword:[self.password copyWithZone:zone]];
    }
    
    return copy;
}

@end
