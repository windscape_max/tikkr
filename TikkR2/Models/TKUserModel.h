//
//  TKUserModel.h
//  TikkR2
//
//  Created by Yura on 11/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TKUserLocalModel;

@interface TKUserModel : NSObject<EKMappingProtocol, NSCopying>

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSDate *birthday;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *passCode;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *token;

@property (nonatomic, strong) TKUserLocalModel *local;

@end
