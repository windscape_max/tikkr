//
//  TKMomentBeneficiary.m
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKMomentBeneficiary.h"

@implementation TKMomentBeneficiary

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"idNumber", @"name"]];
    }];
}

- (NSString*)description {
    NSMutableString *descr = [NSMutableString new];
    [descr appendFormat:@"\nidNumber %ld\n", self.idNumber];
    [descr appendFormat:@"name %@\n", self.name];
    return descr;
}

@end
