//
//  TKAccident.h
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKMomentRange : NSObject <EKMappingProtocol>

@property (nonatomic) NSInteger to;
@property (nonatomic) NSInteger from;

@end
