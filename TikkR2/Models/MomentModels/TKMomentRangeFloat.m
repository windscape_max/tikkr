//
//  TKMomentRangeFloat.m
//  TikkR2
//
//  Created by Yura on 1/25/17.
//  Copyright © 2017 WallTree. All rights reserved.
//

#import "TKMomentRangeFloat.h"

@implementation TKMomentRangeFloat

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"from", @"to"]];
    }];
}

@end
