//
//  TKMomentBeneficiary.h
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKMomentBeneficiary : NSObject<EKMappingProtocol>

@property (nonatomic) NSInteger idNumber;
@property (nonatomic, copy) NSString* name;

@end
