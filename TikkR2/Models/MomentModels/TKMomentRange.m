//
//  TKAccident.m
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKMomentRange.h"

@implementation TKMomentRange

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"from", @"to"]];
    }];
}

- (NSString*)description {
    NSMutableString *descr = [NSMutableString new];
    [descr appendFormat:@"\nfrom %ld\n", self.from];
    [descr appendFormat:@"to %ld\n", self.to];
    return descr;
}

@end
