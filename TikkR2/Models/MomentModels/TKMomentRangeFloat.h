//
//  TKMomentRangeFloat.h
//  TikkR2
//
//  Created by Yura on 1/25/17.
//  Copyright © 2017 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKMomentRangeFloat : NSObject <EKMappingProtocol>

@property (nonatomic) float to;
@property (nonatomic) float from;

@end
