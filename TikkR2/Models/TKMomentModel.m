//
//  TKMomentModel.m
//  TikkR2
//
//  Created by Yura on 11/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKMomentModel.h"

@implementation TKMomentModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.accident = [TKMomentRange new];
        self.insurance = [TKMomentRange new];
        self.payInsurance = [TKMomentRangeFloat new];
        self.payAccident = [TKMomentRangeFloat new];
    }
    return self;
}

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"_id", @"name", @"img", @"desc"]];
        [mapping hasOne:[TKMomentRange class] forKeyPath:@"accident"];
        [mapping hasOne:[TKMomentRange class] forKeyPath:@"insurance"];
        [mapping hasOne:[TKMomentRangeFloat class] forKeyPath:@"payInsurance"];
        [mapping hasOne:[TKMomentRangeFloat class] forKeyPath:@"payAccident"];
    }];
}

- (NSString*)description {
    NSMutableString *descr = [NSMutableString new];
    [descr appendFormat:@"\n _id %@\n", self._id];
    [descr appendFormat:@"name %@\n", self.name];
    [descr appendFormat:@"img %@\n", self.img];
    [descr appendFormat:@"accident %@\n", self.accident];
    [descr appendFormat:@"insurance %@\n", self.insurance];
    [descr appendFormat:@"payInsurance %@\n", self.payInsurance];
    [descr appendFormat:@"payAccident %@\n", self.payAccident];
    return descr;
}


@end
