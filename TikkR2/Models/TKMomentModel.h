//
//  TKMomentModel.h
//  TikkR2
//
//  Created by Yura on 11/25/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TKMomentRange.h"
#import "TKMomentRangeFloat.h"

@interface TKMomentModel : NSObject<EKMappingProtocol>

@property (nonatomic, copy) NSString *_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *desc;

@property (nonatomic, strong) TKMomentRange *accident;
@property (nonatomic, strong) TKMomentRange *insurance;
@property (nonatomic, strong) TKMomentRangeFloat *payInsurance;
@property (nonatomic, strong) TKMomentRangeFloat *payAccident;

+ (EKObjectMapping *)objectMapping;

@end
