//
//  TKUserLocalModel.h
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKUserLocalModel : NSObject<EKMappingProtocol, NSCopying>

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;

@end
