//
//  TKSelectedMomentTableViewCell.m
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKSelMomentTableViewCell.h"
#import "TKActiveMomentModel.h"
#import "TKMomentModel.h"

@interface TKSelMomentTableViewCell () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageLeadingConstraint;

@end

@implementation TKSelMomentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.recognizerSwipe1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(actionSwipeCell:)];
    self.recognizerSwipe1.direction = UISwipeGestureRecognizerDirectionLeft;
    self.recognizerSwipe1.enabled = YES;
    [self addGestureRecognizer:self.recognizerSwipe1];
    
    self.userInteractionEnabled = YES;
    self.img.userInteractionEnabled = YES;
    self.lblDescription.userInteractionEnabled = YES;
    self.titleLabel.userInteractionEnabled = YES;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)actionSwipeCell:(UISwipeGestureRecognizer*)recognizer {
    
    if (!_canSwipe) return;
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        //self.activeFlag = !self.activeFlag;
        
         if (self.activeFlag)
         self.mainView.backgroundColor = CELL_ACTIVE_COLOR;
         else
         self.mainView.backgroundColor = CELL_DEACTIVE_COLOR;
         
#define K_ANIM_SPEED 0.22
#define K_ANIM_RETURN_SPEED 0.15
#define K_ANIM_SCALE 40.0
        
        self.mainViewTrailingConstraint.constant += K_ANIM_SCALE;
        self.imageLeadingConstraint.constant -= K_ANIM_SCALE;
        [UIView animateWithDuration:K_ANIM_SPEED animations:^(void) {
            [self layoutIfNeeded];
        }completion:^(BOOL finished) {
            if (finished) {
                self.mainViewTrailingConstraint.constant -= K_ANIM_SCALE;
                self.imageLeadingConstraint.constant += K_ANIM_SCALE;
                [UIView animateWithDuration:K_ANIM_RETURN_SPEED animations:^(void) {
                    [self layoutIfNeeded];
                }];
                
                [self.delegate StatusChange:self];
            }
        }];
        
        self.lblActivate.text = (self.activeFlag) ? @"SWIPE TO DEACTIVATE" : @"SWIPE TO ACTIVATE";
    }
}

- (void)SetPriceData:(NSNumber *)price {
    NSMutableAttributedString* labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"€ %.2f /hour", [price floatValue]]];
    // NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:@"€ 0.50 /hour"];
    [labelText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Avenir Next" size:10] range:NSMakeRange(0, 1)]; // euro sign
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:18.0] range:NSMakeRange(2, 1)];           // zero symbol
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0] range:NSMakeRange(3, 3)];           // other price
    
    // if (labelText.length == 12) {
    [labelText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Avenir Next" size:10] range:NSMakeRange(7, 5)]; // hour text
    // } else {
    //     [labelText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Avenir Next" size:10] range:NSMakeRange(6, 5)]; // hour text
    // }
    
    self.lblPrice.attributedText = labelText;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (void)UpdateStatus {
    //self.activeFlag = !self.activeFlag;
    
    if (self.activeFlag)
        self.mainView.backgroundColor = CELL_ACTIVE_COLOR;
    else
        self.mainView.backgroundColor = CELL_DEACTIVE_COLOR;
    
    self.lblActivate.text = (self.activeFlag) ? @"SWIPE TO DEACTIVATE" : @"SWIPE TO ACTIVATE";
}

- (void)UpdateUI {
    self.activeFlag = self.model.momentActive;
    self.titleLabel.text = self.model.momentId.name;

    if ([self.model.momentId.name isEqualToString:@"Sports Moment"]) {
        self.img.image = [UIImage imageNamed:@"sport_moments2"];
    }
    
    if ([self.model.momentId.name isEqualToString:@"Travel Moment"]) {
        self.img.image = [UIImage imageNamed:@"travel_moments2"];
    }
    
    if ([self.model.momentId.name isEqualToString:@"Commute Moment"]) {
        self.img.image = [UIImage imageNamed:@"commute_moments2"];
    }
    
    self.contentView.backgroundColor = [UIColor clearColor];
    
    NSNumber *numba = [NSNumber numberWithFloat:self.model.lifePay + self.model.accidentPay];
    [self SetPriceData:numba]; // JUST DO IT
    [self UpdateStatus];
    
    self.lblDescription.text = self.model.momentId.desc;
}

@end

