//
//  TKMomentTableViewCell.h
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TKMomentModel;

@interface TKMomentTableViewCell : UITableViewCell

@property (nonatomic) BOOL canSwipe;
@property (nonatomic, strong) TKMomentModel *model;
@property (weak, nonatomic) IBOutlet UIView *lineView;

- (void)updateUI;

@end
