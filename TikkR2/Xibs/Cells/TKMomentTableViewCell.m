//
//  TKMomentTableViewCell.m
//  TikkR2
//
//  Created by Yura on 12/8/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKMomentTableViewCell.h"
#import "TKMomentModel.h"

@interface TKMomentTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imgMoment;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation TKMomentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI {
    self.lblTitle.text = self.model.name;
    
    if ([self.model.name isEqualToString:@"Sports Moment"]) {
        self.imgMoment.image = [UIImage imageNamed:@"sport_moments"];
    }
    
    if ([self.model.name isEqualToString:@"Travel Moment"]) {
        self.imgMoment.image = [UIImage imageNamed:@"travel_moments"];
    }
    
    if ([self.model.name isEqualToString:@"Commute Moment"]) {
        self.imgMoment.image = [UIImage imageNamed:@"commute_moments"];
    }
    
    self.contentView.backgroundColor = [UIColor clearColor];
}

@end
