//
//  TKNetworkServiceIMF.h
//  TikkR2
//
//  Created by Yura on 12/12/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKNetworkBaseIMF.h"
#import "TKNetworkServiceProtocol.h"

@interface TKNetworkServiceIMF : TKNetworkBaseIMF<TKNetworkServiceProtocol>

@end
