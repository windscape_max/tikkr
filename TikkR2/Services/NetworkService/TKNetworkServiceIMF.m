//
//  TKNetworkServiceIMF.m
//  TikkR2
//
//  Created by Yura on 12/12/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKNetworkServiceIMF.h"
#import "TKNetworkConstants.h"
#import "TKHelper.h"
#import "TKUserModel.h"

@interface TKNetworkServiceIMF ()

@end

@implementation TKNetworkServiceIMF

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (BOOL)validateResponseWithResponse:(id)response{
    if ([[response objectForKey:@"type"] isEqualToString:@"error"]) {
        [TKHelper ShowMessageWithTitle:[response objectForKey:@"notice"]];
        return NO;
    }
    if ([[response objectForKey:@"type"] isEqualToString:@"info"]) {
        return NO;
    }
    return YES;
}

#pragma mark -
#pragma mark - Token methods

- (void)authWithFBAndCompletion:(void(^)(BOOL sucess))completion {
    
}



#pragma mark -
#pragma mark - API Auth

- (void)getUserInfoWithCompletion:(void (^)(BOOL, id))completion {
    
}


#pragma mark -
#pragma mark - API Registration

- (void)regSendPhoneNumber:(NSString *)phone andCompletion:(void (^)(BOOL, id))completion {
    
}

- (void)regCheckCode:(NSString *)code andPhone:(NSString *)phone andCompletion:(void (^)(BOOL, id))completion {
    
}

- (void)regUserRegisterWithUserModel:(TKUserModel *)model andCompletion:(void (^)(BOOL, id))completion {
    
}



#pragma mark -
#pragma mark - API Edit Profil



#pragma mark -
#pragma mark - API Moments

- (void)getUserMomentsWithCompletion:(void (^)(BOOL sucsess, id response))completion {
    /*
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_MOMENTS_GET];
    [self performRequest:nil andEndpoint:path andMethod:GET success:^(id response) {
        [self validateResponseWithResponse:response];
        completion(YES, response);
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
     */
}

- (void)getUserActiveMomentsWithCompletion:(void (^)(BOOL, id))completion{
    /*
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_MOMENTS_GET_ACTIVE];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    [self performRequest:nil andEndpoint:path andMethod:GET success:^(id response) {
        [self validateResponseWithResponse:response];
        completion(YES, response);
    } fail:^(id response) {
        completion(NO, response);
    }];
    */
}

#pragma mark -
#pragma mark - API Claims



#pragma mark -
#pragma mark - API Chat



#pragma mark -
#pragma mark - API Transactions


@end
