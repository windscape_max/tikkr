//
//  TKNetworkService.m
//  TikkR2
//
//  Created by Yura on 12/12/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKNetworkService.h"
#import "TKNetworkServiceAF.h"
#import "TKNetworkServiceIMF.h"

#import "TKServiceManager.h"
#import "TKStorageService.h"
#import "TKHelper.h"

#import "TKUserModel.h"

@interface TKNetworkService ()

@property (nonatomic, strong) TKNetworkServiceAF   *networkServiceAF;
@property (nonatomic, strong) TKNetworkServiceIMF  *networkServiceIMF;

@end

@implementation TKNetworkService

- (instancetype)init {
    self = [super init];
    if (self) {
        self.networkServiceAF = [[TKNetworkServiceAF alloc] init];
        self.networkServiceIMF = [[TKNetworkServiceIMF alloc] init];
    }
    return self;
}

- (void)setToken:(NSString *)token {
    [self.networkServiceAF setToken:token];
}


#pragma mark -
#pragma mark - Token methods

// show FB auth with imf service
// auth with fb token to our server

- (void)authWithFBAndCompletion:(void(^)(BOOL sucess))completion {
    NSString *requestPath = [NSString stringWithFormat:@"%@/protected", [[IMFClient sharedInstance] backendRoute]];
    [self.networkServiceIMF performRequest:nil andEndpoint:requestPath andMethod:GET success:^(id response) {
        
        [self.networkServiceIMF authWithFBAndCompletion:^(BOOL sucess) {
            completion(sucess);
        }];
        
    } fail:^(id response) {
        completion(response);
    }];
}

// get google access token via AppAuth
// send access token to our server

- (void)authWithGoogleAndCompletion:(void(^)(BOOL sucess))completion inViewController:(UIViewController*)vc {
    [self.networkServiceAF authWithGoogleAndCompletion:^(BOOL sucess) {
        completion(sucess);
    } inViewController:vc];
}

#pragma mark -
#pragma mark - API Auth

- (void)authWithEmail:(NSString*)email andPassword:(NSString*)password andCompletion:(void(^)(BOOL sucess, id response))completion {
    [self.networkServiceAF authWithEmail:email andPassword:password andCompletion:^(BOOL sucess, id response) {
        completion(sucess, response);
    }];
}

- (void)getUserInfoWithCompletion:(void (^)(BOOL, id))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        [self.networkServiceIMF getUserInfoWithCompletion:^(BOOL sucess, id response) {
            completion(sucess, response);
        }];
    } else {
        [self.networkServiceAF getUserInfoWithCompletion:^(BOOL sucess, id response) {
            completion(sucess, response);
        }];
    }
}

#pragma mark -
#pragma mark - API Registration

- (void)regSendPhoneNumber:(NSString *)phone andCompletion:(void (^)(BOOL, id))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        [self.networkServiceIMF regSendPhoneNumber:phone andCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    } else {
        [self.networkServiceAF regSendPhoneNumber:phone andCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    }
}

- (void)regCheckCode:(NSString *)code andPhone:(NSString *)phone andCompletion:(void (^)(BOOL, id))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        [self.networkServiceIMF regCheckCode:code andPhone:phone andCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    } else {
        [self.networkServiceAF regCheckCode:code andPhone:phone andCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    }
}


#pragma mark -
#pragma mark - API Edit Profil



#pragma mark -
#pragma mark - API Moments

- (void)getUserMomentsWithCompletion:(void(^)(BOOL sucess, id response))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        [self.networkServiceIMF getUserMomentsWithCompletion:^(BOOL sucess, id response) {
            completion(sucess, response);
        }];
    } else {
        [self.networkServiceAF getUserMomentsWithCompletion:^(BOOL sucess, id response) {
            completion(sucess, response);
        }];
    }
}

- (void)getUserActiveMomentsWithCompletion:(void(^)(BOOL sucess, id response))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        [self.networkServiceIMF getUserActiveMomentsWithCompletion:^(BOOL sucess, id response) {
            completion(sucess, response);
        }];
    } else {
        [self.networkServiceAF getUserActiveMomentsWithCompletion:^(BOOL sucess, id response) {
            completion(sucess, response);
        }];
    }
}

- (void)regUserRegisterWithUserModel:(TKUserModel *)model andCompletion:(void (^)(BOOL, id))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        [self.networkServiceIMF regUserRegisterWithUserModel:model andCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    } else {
        [self.networkServiceAF regUserRegisterWithUserModel:model andCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    }
}

- (void)changeUserMomentWithMoment:(AccidentData *)moment andCompletion:(void (^)(BOOL, id))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        ///
    } else {
        [self.networkServiceAF changeUserMomentWithMoment:moment andCompletion:^(BOOL sucess, id response) {
            completion(sucess, response);
        }];
    }
}

- (void)setActiveMomentWithMomentId:(NSString *)momentId andStatus:(BOOL)status ancCompletion:(void (^)(BOOL, id))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        ///
    } else {
        [self.networkServiceAF setActiveMomentWithMomentId:momentId andStatus:status ancCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    }
}

- (void)removeActiveMomentWithMomentId:(NSString *)momentId andCompletion:(void (^)(BOOL, id))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        ///
    } else {
        [self.networkServiceAF removeActiveMomentWithMomentId:momentId andCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    }
}

- (void)updateUserMomentWithAccidentData:(AccidentData *)data andMoment:(MomentData *)moment andCompletion:(void (^)(BOOL, id))completion {
    if ([self.storageService.registerViaService isEqualToString:@"FB"]) {
        ///
    } else {
        [self.networkServiceAF updateUserMomentWithAccidentData:data andMoment:moment andCompletion:^(BOOL success, id response) {
            completion(success, response);
        }];
    }
}

#pragma mark -
#pragma mark - API Claims



#pragma mark -
#pragma mark - API Chat



#pragma mark -
#pragma mark - API Transactions



@end


