//
//  TKNetworkServiceProtocol.h
//  TikkR2
//
//  Created by Yura on 12/12/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "AccidentData.h"
#import "MomentData.h"

@class TKUserModel;
@protocol TKNetworkServiceProtocol <NSObject>


#pragma mark -
#pragma mark - Token methods

@optional
- (void)authWithFBAndCompletion:(void(^)(BOOL sucess))completion;
- (void)authWithGoogleAndCompletion:(void(^)(BOOL sucess))completion inViewController:(UIViewController*)vc;
- (void)AuthWithGoogleTokenAndCompletion:(void(^)(BOOL sucess))completion;

#pragma mark -
#pragma mark - API Auth

- (void)authWithEmail:(NSString*)email andPassword:(NSString*)password andCompletion:(void(^)(BOOL sucess, id response))completion;


@required

- (void)getUserInfoWithCompletion:(void(^)(BOOL sucess, id response))completion;

#pragma mark -
#pragma mark - API Registration

- (void)regSendPhoneNumber:(NSString*)phone andCompletion:(void(^)(BOOL success, id response))completion;
- (void)regCheckCode:(NSString*)code andPhone:(NSString*)phone andCompletion:(void(^)(BOOL success, id response))completion;
- (void)regUserRegisterWithUserModel:(TKUserModel*)model andCompletion:(void(^)(BOOL success, id response))completion;

#pragma mark -
#pragma mark - API Edit Profil



#pragma mark -
#pragma mark - API Moments

- (void)getUserMomentsWithCompletion:(void(^)(BOOL sucess, id response))completion;
- (void)getUserActiveMomentsWithCompletion:(void(^)(BOOL sucess, id response))completion;
- (void)changeUserMomentWithMoment:(AccidentData*)moment andCompletion:(void(^)(BOOL sucess, id response))completion;
- (void)setActiveMomentWithMomentId:(NSString*)momentId andStatus:(BOOL)status ancCompletion:(void(^)(BOOL success, id response))completion;
- (void)removeActiveMomentWithMomentId:(NSString*)momentId andCompletion:(void(^)(BOOL success, id response))completion;
- (void)updateUserMomentWithAccidentData:(AccidentData*)data andMoment:(MomentData*)moment andCompletion:(void(^)(BOOL success, id response))completion;

#pragma mark -
#pragma mark - API Claims



#pragma mark -
#pragma mark - API Chat



#pragma mark -
#pragma mark - API Transactions



@end

