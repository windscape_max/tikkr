//
//  TKNetworkService.h
//  TikkR2
//
//  Created by Yura on 12/12/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKNetworkServiceProtocol.h"

@class TKStorageService;

@interface TKNetworkService : NSObject <TKNetworkServiceProtocol>

@property (nonatomic, weak) TKStorageService *storageService;

- (void)setToken:(NSString*)token;

@end
