//
//  TKNetworkService.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKNetworkServiceAF.h"
#import "TKAppDelegate.h"
#import "TKNetworkConstants.h"
#import "TKHelper.h"
#import "TKUserModel.h"
#import "TKUserLocalModel.h"
#import "MomentData.h"

@interface TKNetworkServiceAF ()  < OIDAuthStateChangeDelegate, OIDAuthStateErrorDelegate >
@property (nonatomic, strong, nullable) OIDAuthState *authState;
@end

@implementation TKNetworkServiceAF

- (instancetype)init {
    self = [super init];
    if (self) {
        //self.token = API_MAIN_TEST_TOKEN;
    }
    return self;
}

- (BOOL)validateResponseWithResponse:(id)response{
    [TKHelper DismissHUD];
    if ([[response objectForKey:@"type"] isEqualToString:@"error"]) {
        [TKHelper ShowMessageWithTitle:[response objectForKey:@"notice"]];
        return NO;
    }
    if ([[response objectForKey:@"type"] isEqualToString:@"info"]) {
        return NO;
    }
    return YES;
}

- (BOOL)validateResponseWithResponseWithourDissmissHUD:(id)response{
    if ([[response objectForKey:@"type"] isEqualToString:@"error"]) {
        [TKHelper ShowMessageWithTitle:[response objectForKey:@"notice"]];
        return NO;
    }
    if ([[response objectForKey:@"type"] isEqualToString:@"info"]) {
        return NO;
    }
    return YES;
}


#pragma mark -
#pragma mark - Token functions

- (void)authWithGoogleAndCompletion:(void(^)(BOOL sucess))completion inViewController:(UIViewController*)vc {
    NSURL *issuer = [NSURL URLWithString:API_ISSUER];
    NSURL *redirectURI = [NSURL URLWithString:API_REDIRECT_URI];
    
    // discovers endpoints
    [OIDAuthorizationService discoverServiceConfigurationForIssuer:issuer
                                                        completion:^(OIDServiceConfiguration *_Nullable configuration, NSError *_Nullable error) {
                                                            
                                                            if (!configuration) {
                                                                NSLog(@"Error retrieving discovery document: %@", [error localizedDescription]);
                                                                [self setAuthState:nil];
                                                                completion(FALSE);
                                                                return;
                                                            }
                                                            
                                                            NSLog(@"Got configuration: %@", configuration);
                                                            
                                                            // builds authentication request
                                                            OIDAuthorizationRequest *request =
                                                            [[OIDAuthorizationRequest alloc] initWithConfiguration:configuration
                                                                                                          clientId:API_CLIENT_ID
                                                                                                            scopes:@[OIDScopeOpenID, OIDScopeProfile]
                                                                                                       redirectURL:redirectURI
                                                                                                      responseType:OIDResponseTypeCode
                                                                                              additionalParameters:nil];
                                                            // performs authentication request
                                                            TKAppDelegate *appDelegate = (TKAppDelegate *)[UIApplication sharedApplication].delegate;
                                                            NSLog(@"Initiating authorization request with scope: %@", request.scope);
                                                            
                                                            appDelegate.currentAuthorizationFlow =
                                                            [OIDAuthState authStateByPresentingAuthorizationRequest:request
                                                                                           presentingViewController:vc
                                                                                                           callback:^(OIDAuthState *_Nullable authState,
                                                                                                                      NSError *_Nullable error) {
                                                                                                               if (authState) {
                                                                                                                   [self setAuthState:self.authState];
                                                                                                                   NSLog(@"Got authorization tokens. Access token: %@",
                                                                                                                         authState.lastTokenResponse.accessToken);
                                                                                                                   self.token = authState.lastTokenResponse.accessToken;
                                                                                                                   
                                                                                                                   //NSLog(@"RESPONSE SUKA :: %@", authState.lastTokenResponse);
                                                                                                                   
                                                                                                                   //
                                                                                                                  [self AuthWithGoogleTokenAndCompletion:nil];
                                                                                                                   
                                                                                                                   
                                                                                                               } else {
                                                                                                                   NSLog(@"Authorization error: %@", [error localizedDescription]);
                                                                                                                   [self setAuthState:nil];
                                                                                                                   completion(FALSE);
                                                                                                               }
                                                                                                           }];
                                                        }];

}

- (void)AuthWithGoogleTokenAndCompletion:(void(^)(BOOL sucess))completion {
    [self performRequest:@{ } andEndpoint:[NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_GOOGLE_AUTH] andMethod:GET success:^(id response) {
        NSLog(@"Success :: %@", (NSDictionary*)response);
    }fail:^(id response) {
        NSLog(@"Error :: %@", (NSDictionary*)response);
    }];
}


#pragma mark -
#pragma mark - Google oAuth Delegate

- (void)didChangeState:(OIDAuthState *)state {
    // NSLog(@"DID RECEIVE OAUTH IO RESPONCE :: %@" , request.data.access_token);
}

- (void)authState:(OIDAuthState *)state didEncounterAuthorizationError:(NSError *)error {
    
}



#pragma mark -
#pragma mark - API Auth

- (void)authWithEmail:(NSString*)email andPassword:(NSString*)password andCompletion:(void(^)(BOOL sucess, id response))completion {
    [TKHelper ShowLoadingHUD];
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_AUTH_LOGIN];
    [self performRequest:@{@"email": email, @"password" : password} andEndpoint:path andMethod:POST success:^(id response) {
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
}

- (void)getUserInfoWithCompletion:(void (^)(BOOL, id))completion {
    [TKHelper ShowLoadingHUD];
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_GET_USER_INFO];
    [self performRequest:nil andEndpoint:path andMethod:GET success:^(id response) {
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
}



#pragma mark -
#pragma mark - API Registration

- (void)regSendPhoneNumber:(NSString *)phone andCompletion:(void (^)(BOOL, id))completion {
    [TKHelper ShowLoadingHUD];
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_REG_SEND_PHONE_NUMBER];
    [self performRequest:@{@"number": phone} andEndpoint:path andMethod:POST success:^(id response) {
       // [TKHelper DismissHUD];
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
}

- (void)regCheckCode:(NSString *)code andPhone:(NSString *)phone andCompletion:(void (^)(BOOL, id))completion {
    [TKHelper ShowLoadingHUD];
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_REG_CHECK_CODE];
    [self performRequest:@{@"number": phone, @"code" : code} andEndpoint:path andMethod:POST success:^(id response) {
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
}

- (void)regUserRegisterWithUserModel:(TKUserModel *)model andCompletion:(void (^)(BOOL, id))completion {
    [TKHelper ShowLoadingHUD];
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_REG_REGISTER_USER_EMAIL];
    [self performRequest:@{@"name": model.name,
                           @"birthday" : model.birthday,
                           @"phone" : model.phone,
                           @"passCode" : model.passCode,
                           @"email" : model.local.email,
                           @"password" : model.local.password} andEndpoint:path andMethod:POST success:^(id response) {
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
}



#pragma mark -
#pragma mark - API Edit Profil



#pragma mark -
#pragma mark - API Moments

- (void)getUserMomentsWithCompletion:(void (^)(BOOL sucsess, id response))completion {
    [TKHelper ShowLoadingHUD];
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_MOMENTS_GET];
    [self performRequest:nil andEndpoint:path andMethod:GET success:^(id response) {
        if (![self validateResponseWithResponseWithourDissmissHUD:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
}

- (void)getUserActiveMomentsWithCompletion:(void (^)(BOOL, id))completion{
   // [TKHelper ShowLoadingHUD];
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_MOMENTS_GET_ACTIVE];

    [self performRequest:nil andEndpoint:path andMethod:GET success:^(id response) {
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
}

- (void)changeUserMomentWithMoment:(AccidentData *)moment andCompletion:(void (^)(BOOL, id))completion {
    [TKHelper ShowLoadingHUD];
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_MOMENTS_CHANGE];
    NSDictionary *parameters = @{ @"momentId" : moment.momentID,
                                  @"accidentPay" : @(moment.accidentPay),
                                  @"accidentInsurance" : @(moment.accidentInsurance),
                                  @"lifePay" : @(moment.lifePay),
                                  @"lifeInsurance" : @(moment.lifeInsurance),
                                  @"disabilityByAccident" : @(moment.disabilityByAccident),
                                  @"hospitalization" : @(moment.hospitalization),
                                  @"deathByAccident" : @(moment.deathByAccident),
                                  @"smoke" : @(moment.smoke),
                                  @"disabilityAbsolute" : @(moment.disabilityAbsolute),
                                  @"totalDisability" : @(moment.totalDisability),
                                  @"funeralBenefit" : @(moment.funeralBenefit),
                                  @"momentActive" : @(moment.momentActive)
                                 };
    
    [self performRequest:parameters andEndpoint:path andMethod:POST success:^(id response) {
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        [TKHelper DismissHUD];
        completion(NO, response);
    }];
}

- (void)setActiveMomentWithMomentId:(NSString *)momentId andStatus:(BOOL)status ancCompletion:(void (^)(BOOL, id))completion {
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_MOMENTS_CHANGE_STATUS];
    [self performRequest:@{ @"userMomentId" : momentId,
                            @"status" : @(status) } andEndpoint:path andMethod:PUT success:^(id response) {
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        completion(NO, response);
    }];
}

- (void)removeActiveMomentWithMomentId:(NSString *)momentId andCompletion:(void (^)(BOOL, id))completion {
    NSString *path = [NSString stringWithFormat:@"%@%@%@", API_APP_ROUTE, API_MOMENTS_REMOVE, momentId];
    [self performRequest:nil andEndpoint:path andMethod:DELETE success:^(id response) {
                                if (![self validateResponseWithResponse:response]) {
                                    completion(YES, nil);
                                } else {
                                    completion(YES, response);
                                }
                            } fail:^(id response) {
                                completion(NO, response);
                            }];
}

- (void)updateUserMomentWithAccidentData:(AccidentData *)data andMoment:(MomentData*)moment andCompletion:(void (^)(BOOL, id))completion {
    NSString *path = [NSString stringWithFormat:@"%@%@", API_APP_ROUTE, API_MOMENTS_UPDATE];
    
    NSDictionary *parameters = @{ @"userMomentId" : data.momentID,
                                  @"accidentPay" : @(data.accidentPay),
                                  @"accidentInsurance" : @(data.accidentInsurance),
                                  @"lifePay" : @(data.lifePay),
                                  @"lifeInsurance" : @(data.lifeInsurance),
                                  @"disabilityByAccident" : @(data.disabilityByAccident),
                                  @"hospitalization" : @(data.hospitalization),
                                  @"deathByAccident" : @(data.deathByAccident),
                                  @"smoke" : @(data.smoke),
                                  @"disabilityAbsolute" : @(data.disabilityAbsolute),
                                  @"totalDisability" : @(data.totalDisability),
                                  @"funeralBenefit" : @(data.funeralBenefit),
                                  @"momentActive" : @(data.momentActive)
                                  };
    
    [self performRequest:parameters andEndpoint:path andMethod:PUT success:^(id response) {
        if (![self validateResponseWithResponse:response]) {
            completion(YES, nil);
        } else {
            completion(YES, response);
        }
    } fail:^(id response) {
        completion(NO, response);
    }];
}

#pragma mark -
#pragma mark - API Claims



#pragma mark -
#pragma mark - API Chat



#pragma mark -
#pragma mark - API Transactions



@end
