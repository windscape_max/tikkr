//
//  TKNetworkBaseIMF.m
//  TikkR2
//
//  Created by Yura on 12/12/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKNetworkBaseIMF.h"

@implementation TKNetworkBaseIMF

- (void)performRequest:(NSDictionary *)data andEndpoint:(NSString *)endpoint andMethod:(enum REQUEST_METHOD)method success:(void(^)(id response))success fail:(void(^)(id response))fail
{
    if(data == nil)
        data = @{};
    
    [self requestWithMethod:method path:endpoint parameters:data success:success fail:fail];
}

- (void)requestWithMethod:(enum REQUEST_METHOD)method path:endpoint parameters:data success:(void(^)(id response))success fail:(void(^)(id response))fail{
    
    IMFResourceRequest *request = nil;
    
    switch (method)
    {
        case GET:
            request =  [IMFResourceRequest requestWithPath:endpoint method:@"GET"];
            break;
        case POST:
            request =  [IMFResourceRequest requestWithPath:endpoint method:@"POST"];
            break;
        case DELETE:
            request =  [IMFResourceRequest requestWithPath:endpoint method:@"DELETE"];
            break;
        case PUT:
            request =  [IMFResourceRequest requestWithPath:endpoint method:@"PUT"];
            break;
        default:
            break;
    }
    
    [request sendWithCompletionHandler:^(IMFResponse *response, NSError *error) {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        if (error) {
#ifdef DEBUG
            NSLog(@"=> SUCCESS :: %@ :: \n%@", [endpoint uppercaseString], response.responseJson);
#endif
            [self callingSuccesses:POST withResponse:response.responseJson endpoint:endpoint data:data success:success fail:fail];
        } else {
#ifdef DEBUG
            NSLog(@"=> ERROR :: %@ :: \n%@", [endpoint uppercaseString], [error description]);
#endif
            [self callingFail:fail error:error];
        }
    }];
}

- (void)callingSuccesses:(enum REQUEST_METHOD)requestMethod withResponse:(id)responseObject endpoint:(NSString *)endpoint data:(NSDictionary *)data success:(void(^)(id response))success fail:(void(^)(id response))fail {
    
    if(success!=nil)
        success(responseObject);
}

- (void)callingFail:(void(^)(id response))fail error:(NSError *)error {
    if(fail!=nil)
        fail(error);
}

@end
