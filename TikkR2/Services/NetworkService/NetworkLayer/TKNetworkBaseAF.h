//
//  TKNetworkAF.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

static NS_ENUM(NSInteger, REQUEST_METHOD)
{
    GET,
    HEAD,
    POST,
    PUT,
    PATCH,
    DELETE
};


@interface TKNetworkBaseAF : AFHTTPRequestOperationManager

@property (nonatomic, strong) NSString *token;

- (void)performRequest:(NSDictionary *)data andEndpoint:(NSString *)endpoint andMethod:(enum REQUEST_METHOD)method success:(void(^)(id response))success fail:(void(^)(id response))fail;

@end
