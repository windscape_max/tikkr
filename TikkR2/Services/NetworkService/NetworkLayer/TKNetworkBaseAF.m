//
//  TKNetworkAF.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKNetworkBaseAF.h"

@implementation TKNetworkBaseAF

- (void)performRequest:(NSDictionary *)data andEndpoint:(NSString *)endpoint andMethod:(enum REQUEST_METHOD)method success:(void(^)(id response))success fail:(void(^)(id response))fail
{
    if (self.token.length > 0) {
        //self.requestSerializer = [AFJSONRequestSerializer serializer];
        //[self.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [self.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", self.token] forHTTPHeaderField:@"Authorization"];
    }
    
    if(data == nil)
        data = @{};
    
    AFHTTPRequestOperation *operation = [self requestWithMethod:method path:endpoint parameters:data success:success fail:fail];
    [operation start];
}

- (AFHTTPRequestOperation *)requestWithMethod:(enum REQUEST_METHOD)method path:endpoint parameters:data success:(void(^)(id response))success fail:(void(^)(id response))fail{
    switch (method)
    {
        case GET:
            return [self requestGETMethod:data andEndpoint:endpoint success:success fail:fail];
        case POST:
            return [self requestPOSTMethod:data andEndpoint:endpoint  success:success fail:fail];
        case DELETE:
            return [self requestDELETEMethod:data andEndpoint:endpoint success:success fail:fail];
        case PUT:
            return [self requestPUTMethod:data andEndpoint:endpoint success:success fail:fail];
        default:
            return  nil;
    }
}

- (AFHTTPRequestOperation *)requestGETMethod:(NSDictionary *)data andEndpoint:(NSString *)endpoint success:(void(^)(id response))success fail:(void(^)(id response))fail
{
    return [self GET:endpoint
          parameters:data
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 [self callingSuccesses:GET withResponse:responseObject endpoint:endpoint data:data success:success fail:fail];
                 //  [KNZHttpCache cacheResponse:responseObject httpResponse:operation.response];
                 
#ifdef DEBUG
                 NSLog(@"=> GET SUCCESS :: %@ :: \n%@", [endpoint uppercaseString], responseObject);
#endif
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [self callingFail:fail error:error];
                 
#ifdef DEBUG
                 NSLog(@"=> GET ERROR :: %@ :: \n%@", [endpoint uppercaseString], [error description]);
#endif
             }];
    
}

- (AFHTTPRequestOperation *)requestPOSTMethod:(NSDictionary *)data andEndpoint:(NSString *)endpoint success:(void(^)(id response))success fail:(void(^)(id response))fail {
    return [self POST:endpoint
           parameters:data
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef DEBUG
                  NSLog(@"=> POST SUCCESS :: %@ :: \n%@", [endpoint uppercaseString], responseObject);
#endif
                  [self callingSuccesses:POST withResponse:responseObject endpoint:endpoint data:data success:success fail:fail];
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
                  NSLog(@"=> POST ERROR :: %@ :: \n%@", [endpoint uppercaseString], [error description]);
#endif
                  [self callingFail:fail error:error];
              }];
}

- (AFHTTPRequestOperation *)requestDELETEMethod:(NSDictionary *)data andEndpoint:(NSString *)endpoint success:(void(^)(id response))success fail:(void(^)(id response))fail {
    return [self DELETE:endpoint
           parameters:data
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef DEBUG
                  NSLog(@"=> DELETE SUCCESS :: %@ :: \n%@", [endpoint uppercaseString], responseObject);
#endif
                  [self callingSuccesses:DELETE withResponse:responseObject endpoint:endpoint data:data success:success fail:fail];
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
                  NSLog(@"=> DELETE ERROR :: %@ :: \n%@", [endpoint uppercaseString], [error description]);
#endif
                  [self callingFail:fail error:error];
              }];
}

- (AFHTTPRequestOperation *)requestPUTMethod:(NSDictionary *)data andEndpoint:(NSString *)endpoint success:(void(^)(id response))success fail:(void(^)(id response))fail {
    return [self PUT:endpoint
             parameters:data
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef DEBUG
                    NSLog(@"=> PUT SUCCESS :: %@ :: \n%@", [endpoint uppercaseString], responseObject);
#endif
                    [self callingSuccesses:PUT withResponse:responseObject endpoint:endpoint data:data success:success fail:fail];
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
                    NSLog(@"=> PUT ERROR :: %@ :: \n%@", [endpoint uppercaseString], [error description]);
#endif
                    [self callingFail:fail error:error];
                }];
}

- (void)callingSuccesses:(enum REQUEST_METHOD)requestMethod withResponse:(id)responseObject endpoint:(NSString *)endpoint data:(NSDictionary *)data success:(void(^)(id response))success fail:(void(^)(id response))fail {
    
    if(success!=nil)
        success(responseObject);
}

- (void)callingFail:(void(^)(id response))fail error:(NSError *)error {
    if(fail!=nil)
        fail(error);
}


@end
