//
//  TKNetworkBaseIMF.h
//  TikkR2
//
//  Created by Yura on 12/12/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IMFCore/IMFCore.h>
#import "TKNetworkBaseAF.h" // enum REQUEST_METHOD

@interface TKNetworkBaseIMF : NSObject

- (void)performRequest:(NSDictionary *)data andEndpoint:(NSString *)endpoint andMethod:(enum REQUEST_METHOD)method success:(void(^)(id response))success fail:(void(^)(id response))fail;

@end
