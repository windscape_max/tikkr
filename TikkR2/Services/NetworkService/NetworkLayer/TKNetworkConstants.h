//
//  NetworkConstants.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#ifndef _TK_NETWORK_CONSTANTS_H_
#define _TK_NETWORK_CONSTANTS_H_


#pragma mark -
#pragma marl - General

#define API_APP_ROUTE                       @"https://whispering-beyond-84120.herokuapp.com"

/*
#ifdef DEBUG
    //#define API_APP_ROUTE                   @"http://192.168.88.23:5000"
    #define API_CHAT_ROUTE                  @""
#else
   // #define API_APP_ROUTE                   @"https://dev2-tikkr.mybluemix.net"
    #define API_CHAT_ROUTE                  @""
#endif
 */

#define API_APP_GUID                        @"4c9f4d8c-532c-4fea-8a78-c5ec6fdee801"
#define API_APP_TENAT_ID                    @"1e65f1e3-2503-48e4-97e0-9d5c18c8fa5d"

#define API_APP_IMAGES_ROUTE                @""
#define API_APP_VIDEOS_ROUTE                @""
#define API_APP_FILES_ROUTE                 @""

#define API_MAIN_TEST_TOKEN                 @"4c78feec865dc4e05b8be7c2fb17a394c81d469da38986a282fccadab31d7497"


#pragma mark -
#pragma mark - Google oAuth

#define API_ISSUER                          @"https://accounts.google.com"
#define API_CLIENT_ID                       @"954877403541-bvbm9tevlt9thcu2bvf8f9sjjbpauehb.apps.googleusercontent.com"
#define API_REDIRECT_URI                    @"com.googleusercontent.apps.954877403541-bvbm9tevlt9thcu2bvf8f9sjjbpauehb:/oauthredirect"
#define API_APP_AUTH_STATE                  @"authState"



#pragma mark -
#pragma mark - Facebook oAuth

#define API_GOOGLE_AUTH                     @"/auth/google/token"
#define API_FACEBOOK_AUTH                   @"/auth/facebook"
#define API_FACEBOOK_PUBLIC_KEY             @"6R_QWCLpx_97gO7zVE28IPVNMXI"



#pragma mark - 
#pragma mark - API Auth

#define API_GET_USER_INFO                   @"/v1/user/"
#define API_AUTH_WITH_SERVICE               @"/v1/user"
#define API_AUTH_LOGIN                      @"/v1/user/login"



#pragma mark -
#pragma mark - API Registration

#define API_REG_SEND_PHONE_NUMBER           @"/v1/phone/"
#define API_REG_CHECK_CODE                  @"/v1/phone/check"
#define API_REG_REGISTER_USER_EMAIL         @"/v1/user/profile/email"
#define API_REG_REGISTER_USER_FB            @""
#define API_REG_DELETE_PHONE_IF_NEEDED      @"/v1/user/checkFinishReg" ///////////////////



#pragma mark -
#pragma mark - API Edit Profile

#define API_CHANGE_PASSCODE                 @"/v1/user/passcode"
#define API_CHANGE_USER_INFO                @"/v1/user/"



#pragma mark -
#pragma mark - API Moments

#define API_MOMENTS_CHANGE_STATUS           @"/v1/userMoment/changeActive"
#define API_MOMENTS_GET                     @"/v1/moment"
#define API_MOMENTS_GET_ACTIVE              @"/v1/userMoment/"
#define API_MOMENTS_REMOVE                  @"/v1/userMoment/"
#define API_MOMENTS_CHANGE                  @"/v1/userMoment/" // POST
#define API_MOMENTS_UPDATE                  @"/v1/userMoment" // PUT


#pragma mark -
#pragma mark - API Claims

#define API_CLAIMS_GET                      @"/v1/...."



#pragma mark -
#pragma mark - API Chat

#define API_CHAT_GET_HISTORY                @"/v1/.."
#define API_CHAT_ADD_MESSAGE_TO_HISTORY     @"/v1/"



#pragma mark -
#pragma mark - API Transactions







#endif /* _TK_NETWORK_CONSTANTS_H_ */


