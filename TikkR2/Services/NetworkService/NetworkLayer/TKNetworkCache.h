//
//  TKNetworkCache.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

#define memoCache               4 * 1024 * 1024
#define diskCache               20 * 1024 * 1024
#define DISK_CACHES_FILEPATH    @"%@/Library/Caches/httpCache"

void startNetworkCachingSystem() {
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:memoCache
                                                         diskCapacity:diskCache
                                                             diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
}
