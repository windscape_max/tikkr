//
//  TKHelper.h
//  TikkR2
//
//  Created by Yura on 12/5/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKHelper : NSObject

+ (void)ShowErrorAlertWithTitle:(NSString *)title andDescription:(NSString *)descr andViewController:(UIViewController*)vc;
+ (BOOL)isNetworkConnection;
+ (BOOL)isValidPhoneNumber:(NSString*)phoneNumber;
+ (BOOL)isValidEmailAddress:(NSString*)emailAddress;

#pragma mark -
#pragma mark - Progress HUD

+ (void)ShowMessageWithTitle:(NSString*)title;
+ (void)ShowLoadingHUD;
+ (void)DismissHUD;

@end
