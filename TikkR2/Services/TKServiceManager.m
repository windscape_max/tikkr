//
//  TKServiceManager.m
//  TikkR2
//
//  Created by Yura on 11/18/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKServiceManager.h" 

#import "TKChatService.h"
#import "TKGeoService.h"
#import "TKNetworkService.h"
#import "TKNotificationService.h"
#import "TKObjectService.h"
#import "TKNetworkService.h"
#import "TKPaymentService.h"
#import "TKStorageService.h" 

#import "TKUserModel.h"

@implementation TKServiceManager

+ (TKServiceManager*)sharedManager {
    static dispatch_once_t onceToken;
    static TKServiceManager *instance = nil;
    dispatch_once(&onceToken, ^{
        if (!instance) {
            instance = [[TKServiceManager alloc] init];
            instance.storageService = [[TKStorageService alloc] init];
            instance.networkService = [[TKNetworkService alloc] init];
            instance.networkService.storageService = instance.storageService; // add weak ptr to sotrage service
            instance.objectService = [[TKObjectService alloc] init];
            instance.currentUserModel = [[TKUserModel alloc] init];
        }
    });
    return instance;
}


@end
