//
//  TKLocalStorage.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKStorageService.h"

@implementation TKStorageService

- (instancetype)init {
    self = [super init];
    if (self) {
        [A0SimpleKeychain keychain].useAccessControl = YES; // security
    }
    return self;
}

- (void)StoreData {
    [[A0SimpleKeychain keychain] setString:self.token forKey:@"tk"];
    [[A0SimpleKeychain keychain] setString:self.expired_token forKey:@"exp_tk"];
    [[A0SimpleKeychain keychain] setString:self.refresh_token forKey:@"rf_tk"];
    [[A0SimpleKeychain keychain] setString:self.passcode forKey:@"pc"];
    [[A0SimpleKeychain keychain] setString:self.registerViaService forKey:@"regVia"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:self.registrationFailCompletion forKey:@"rc"];
    [defaults setBool:self.useTouchID forKey:@"ut"];
}

- (void)ReadData {
    // defaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.registrationFailCompletion = [defaults boolForKey:@"rc"];      // registration completion
    self.useTouchID = [defaults boolForKey:@"ut"];                      // use touch id
    
    if (![[A0SimpleKeychain keychain] hasValueForKey:@"tk"]) {
        [defaults setBool:YES forKey:@"fa"];
        return;
        
    } else {
        if (![defaults boolForKey:@"fa"]) {
            // clear previus data (app was deleted)
            [self ClearData];
            
        } else {
            // load data
            self.token = [[A0SimpleKeychain keychain] stringForKey:@"tk"];       // get token
            self.expired_token = [[A0SimpleKeychain keychain] stringForKey:@"exp_tk"];     // get email
            self.refresh_token = [[A0SimpleKeychain keychain] stringForKey:@"rf_tk"];    // get password
            self.passcode = [[A0SimpleKeychain keychain] stringForKey:@"pc"];    // get passcode
            self.registerViaService = [[A0SimpleKeychain keychain] stringForKey:@"regVia"]; // ger rigester via service identificator
        }
    }
    
    [defaults setBool:YES forKey:@"fa"]; //
}

- (void)ClearData {
    self.token = @"";
    self.expired_token = @"";
    self.refresh_token = @"";
    self.passcode = @"";
    self.registerViaService = @"";
    [[A0SimpleKeychain keychain] setString:@"" forKey:@"tk"];
    [[A0SimpleKeychain keychain] setString:@"" forKey:@"exp_tk"];
    [[A0SimpleKeychain keychain] setString:@"" forKey:@"rf_tk"];
    [[A0SimpleKeychain keychain] setString:@"" forKey:@"pc"];
    [[A0SimpleKeychain keychain] setString:@"" forKey:@"regVia"];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:nil forKey:@"rc"];
    [defaults setBool:nil forKey:@"ut"];
}

- (BOOL)IsDataValid {
    NSLog(@"%@", self.token);
    if (!self.token || self.token.length == 0) {
        return NO;
    }
    return YES;
}

@end
