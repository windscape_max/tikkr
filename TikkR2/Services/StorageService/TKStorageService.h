//
//  TKLocalStorage.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKStorageService : NSObject

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *expired_token;
@property (nonatomic, strong) NSString *refresh_token;
@property (nonatomic, strong) NSString *passcode;
@property (nonatomic, strong) NSString *registerViaService; // GOOGLE, FB, EMAIL
@property (nonatomic) BOOL              registrationFailCompletion;
@property (nonatomic) BOOL              useTouchID;

- (void)StoreData;
- (void)ReadData;
- (void)ClearData;
- (BOOL)IsDataValid;

@end
