//
//  TKServiceManager.h
//  TikkR2
//
//  Created by Yura on 11/18/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TKChatService;
@class TKStorageService;
@class TKGeoService;
@class TKNotificationService;
@class TKObjectService;
@class TKNetworkService;
@class TKPaymentService;
@class TKUserModel;

@interface TKServiceManager : NSObject

@property (nonatomic, strong) TKUserModel *currentUserModel;

@property (nonatomic, strong) TKChatService *chatService;
@property (nonatomic, strong) TKStorageService *storageService;
@property (nonatomic, strong) TKGeoService *geolocationService;
@property (nonatomic, strong) TKNotificationService *notificationService;
@property (nonatomic, strong) TKObjectService *objectService;
@property (nonatomic, strong) TKNetworkService *networkService;
@property (nonatomic, strong) TKPaymentService *paymentService;

+ (TKServiceManager*)sharedManager;

@end
