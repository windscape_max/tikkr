//
//  TKHelper.m
//  TikkR2
//
//  Created by Yura on 12/5/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKHelper.h"

#include <unistd.h>
#include <netdb.h>

#import <NBPhoneNumberUtil.h>

@implementation TKHelper

+ (void)ShowErrorAlertWithTitle:(NSString *)title andDescription:(NSString *)descr andViewController:(UIViewController*)vc{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:descr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:ok];
    [vc presentViewController:alert animated:YES completion:nil];
}

+ (void)ShowMessageWithTitle:(NSString *)title {
    [KVNProgress showErrorWithStatus:title];

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.39 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [KVNProgress dismiss];
    });
}

+ (void)ShowLoadingHUD {
    [KVNProgress show];
}

+ (void)DismissHUD {
    [KVNProgress dismiss];
}

+ (BOOL)isNetworkConnection
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        return NO;
    }
    else{
        return YES;
    }
}

+ (BOOL)isValidPhoneNumber:(NSString*)phoneNumber {
    BOOL valid = NO;
    
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:phoneNumber
                                 defaultRegion:@"AT" error:&anError];
    if (anError == nil) {
        valid = [phoneUtil isValidNumber:myNumber];
    } else {
        NSLog(@"Error : %@", [anError localizedDescription]);
    }
    
    return valid;
}

+ (BOOL)isValidEmailAddress:(NSString*)emailAddress {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    return [emailTest evaluateWithObject:emailAddress];
}

@end
