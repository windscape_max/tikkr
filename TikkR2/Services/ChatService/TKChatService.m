//
//  TKChatService.m
//  TikkR2
//
//  Created by Yura on 11/18/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKChatService.h"

@interface TKChatService () <SRWebSocketDelegate>
@property (nonatomic, strong) SRWebSocket *mainSocket;
@end

@implementation TKChatService

- (BOOL)startService {
    _mainSocket.delegate = nil;
    [_mainSocket close];
    
    _mainSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:@"wss://echo.websocket.org"]];
    _mainSocket.delegate = self;
    
    [_mainSocket open];
    // [_webSocket sendPing:nil error:NULL];
    
    return YES;
}

#pragma mark -
#pragma mark - SRWebSocketDelegate

- (void)webSocketDidOpen:(SRWebSocket *)webSocket;
{
    NSLog(@"Websocket Connected");
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error;
{
    NSLog(@":( Websocket Failed With Error %@", error);
    self.mainSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(nonnull NSString *)string
{
    NSLog(@"Received \"%@\"", string);
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean;
{
    NSLog(@"WebSocket closed");
    self.mainSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload;
{
    NSLog(@"WebSocket received pong");
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    // for binary data
}


@end
