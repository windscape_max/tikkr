//
//  AppDelegate.m
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "TKAppDelegate.h"
#import "TKRootWireframe.h"
#import "TKNetworkCache.h"

#import "TKLaunchViewController.h"

@interface TKAppDelegate ()

@property (nonatomic, strong) TKRootWireframe *rootWireframe;

@end

@implementation TKAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // initialize caching system
    startNetworkCachingSystem();
    
    // setup root wireframe
    self.rootWireframe = [[TKRootWireframe alloc] init];
    [self.rootWireframe application:application didFinishLaunchingWithOptions:launchOptions andWindow:self.window];
    
    return YES;
}

/*! @brief Handles inbound URLs. Checks if the URL matches the redirect URI for a pending
 AppAuth authorization request.
 */
- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    // Sends the URL to the current authorization flow (if any) which will process it if it relates to
    // an authorization response.
    if ([_currentAuthorizationFlow resumeAuthorizationFlowWithURL:url]) {
        _currentAuthorizationFlow = nil;
        return YES;
    }
    
    // Your additional URL handling (if any) goes here.
    
    return NO;
}

/*! @brief Forwards inbound URLs for iOS 8.x and below to @c application:openURL:options:.
 @discussion When you drop support for versions of iOS earlier than 9.0, you can delete this
 method. NB. this implementation doesn't forward the sourceApplication or annotations. If you
 need these, then you may want @c application:openURL:options to call this method instead.
 */
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [self application:application
                     openURL:url
                     options:@{}];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    [self.rootWireframe applicationWillEnterForeground:application andWindow:self.window];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self.rootWireframe applicationDidEnterBackground:application andWindow:self.window];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    //[self.rootWireframe applicationWillEnterForeground:application andWindow:self.window];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    [self.rootWireframe applicationWillTerminate:application andWindow:self.window];
}


@end
