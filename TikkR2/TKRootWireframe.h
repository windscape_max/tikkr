//
//  TKRootWireframe.h
//  TikkR2
//
//  Created by Yura on 11/17/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKRootWireframe : NSObject

- (void)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions andWindow:(UIWindow*)window;
- (void)applicationDidEnterBackground:(UIApplication *)application andWindow:(UIWindow*)window;
- (void)applicationWillEnterForeground:(UIApplication *)application andWindow:(UIWindow*)window;
- (void)applicationWillTerminate:(UIApplication *)application andWindow:(UIWindow*)window;

@end
