//
//  AccidentData.m
//  TikkR
//
//  Created by Yura on 10/29/16.
//  Copyright © 2016 WallTree. All rights reserved.
//

#import "AccidentData.h"

@implementation AccidentData

@synthesize momentID;
@synthesize accidentPay;
@synthesize accidentInsurance;
@synthesize lifePay;
@synthesize lifeInsurance;

@synthesize disabilityByAccident;
@synthesize hospitalization;
@synthesize deathByAccident;
@synthesize smoke;
@synthesize disabilityAbsolute;
@synthesize totalDisability;
@synthesize funeralBenefit;

@synthesize beneficiaryName;
@synthesize beneficiaryNationalID;


@end
